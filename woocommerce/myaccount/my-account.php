<?php
/**
 * My Account page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
/**
 * My Account navigation.
 * @since 2.6.0
 */
do_action( 'woocommerce_account_navigation' ); ?>

<style>
.woocommerce-MyAccount-navigation ul {
    display: flex;
    justify-content: space-between;
    width: 90%;
    margin: auto;
    padding: 20px 40px;
    border-bottom: 1px solid #c8c8c8;
}

.woocommerce-MyAccount-navigation a {
    font-family: 'Lato-Regular', sans-serif;
    text-decoration: none;
    font-size: 9pt;
    color: #444;
    text-transform: uppercase;
}


#post-6 {
    margin-bottom: 50px;
}

.woocommerce-MyAccount-content {
    width: 90%;
    margin: auto;
    background-color: #f4f4f4;
    padding: 50px;
}

.woocommerce-MyAccount-content p {
    font-family: 'Lato-Regular', sans-serif;
    font-size: 12pt;
}

.woocommerce-MyAccount-navigation .woocommerce-Address-title {
	display: flex;
	justify-content: space-between;
	width: 25%;
}


.woocommerce-MyAccount-navigation .woocommerce-Address-title {
	display: flex;
	margin-top: 25px;
}

.woocommerce-MyAccount-navigation .woocommerce-Address-title h3 {
	text-transform: uppercase; 
	font-size: 12pt;
	font-family: 'Lato-Regular';
	margin-right: 10px;
}

.woocommerce-MyAccount-navigation .woocommerce-Address-title a {
	text-transform: uppercase; 
	font-size: 9pt;
	font-family: 'Lato-Light';
	line-height: 1.7;	
}

.woocommerce-MyAccount-navigation .woocommerce-Address address {
	font-family: 'Lato-Light';
}

.woocommerce-MyAccount-content .button {
	margin: auto !important;
	width: 100%;
}

.woocommerce-MyAccount-navigation .woocommerce-form-row {
	display: flex;
	flex-direction: column !important;
}

.woocommerce-MyAccount-navigation .woocommerce-form-row--first {
	float: left;
}

.woocommerce-MyAccount-navigation .form-row-wide {
	clear: both;
}

.woocommerce-MyAccount-navigation .form-row {
	margin: 0 20px 20px 0 !important;
}

.woocommerce-MyAccount-navigation input[type=text], select, input[type="email" i], input[type="password" i] {
    border: 1px solid #e5e5e5 !important;
    font-size: 10pt;
    font-family: 'Lato-Regular', sans-serif;
    padding: 6px 10px;
    margin-top: 8px;
}

.woocommerce-MyAccount-content fieldset {
	border: 1px solid #c8c8c8;
    padding: 0 30px;
    margin-bottom: 50px;
}

.woocommerce-MyAccount-content fieldset legend {
	text-transform: uppercase;
	font-size: 10pt;
    color: #777;
    font-family:  'Lato-Regular', sans-serif;
    margin-bottom: 20px;
}

.woocommerce-MyAccount-content a {
	color: #777;
}

 input[type=text] {
		width: 100% !important;
	}

</style>

<div class="woocommerce-MyAccount-content">
	<?php
		/**
		 * My Account content.
		 * @since 2.6.0
		 */
		do_action( 'woocommerce_account_content' );
	?>
</div>
