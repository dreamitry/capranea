<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */
defined( 'ABSPATH' ) || exit;
wc_print_notices();
do_action( 'woocommerce_before_cart' ); ?>

<style>

	.entry-title {
    margin-bottom: 50px;
    text-transform: uppercase;
    font-size: 12pt;
    letter-spacing: 5.5pt;
    font-family: 'Garogier', serif;
}

.post-68 {
    margin-top: 50px;
    margin-bottom: 70px;
}

.entry-content {
    width: 100%;
}

.product-thumbnail {
    width: 20%;
    padding-top: 15px;
    padding-right: 50px;
    border-bottom: 1px solid rgba(175, 175, 175, 0.5);
}

thead {
    margin-bottom: 500px;
    line-height: 2 !important;
}

.product-name {
    width: 20%;
    padding-right: 20px;
    border-bottom: 1px solid rgba(175, 175, 175, 0.5);
}

.product-name > a {  
    text-decoration: none;
    color: #000;
    font-family: 'Lato-Regular', Lato, sans-serif;
    font-size: 9.2pt;
}

.product-price {
    width: 15%;
    border-bottom: 1px solid rgba(175, 175, 175, 0.5);
}

.woocommerce-cart-form__cart-item,  .cart_item {
    background-color: #f4f4f4;
    font-size: 9.4pt;
    font-family: 'Lato-Regular', sans-serif;
}

.product-quantity {
    border-bottom: 1px solid rgba(175, 175, 175, 0.5);
}

.quantity label {
    font-size: 9.2pt !important;
}

.woocommerce {
    margin-bottom: 40px;
    position: relative;
}

.cart-collaterals {
    padding: 30px;
    /*background-color: #ccc;*/
    border-left: 1px solid rgba(175, 175, 175, 0.5);
	position: relative;
    left: 70%;
}



.cart_totals h2 {
    font-size: 14pt;
    margin-bottom: 53px;
    margin-top: -27px;
}

.cart_totals .shop_table, .cart_totals .shop_table_responsive {
	margin-bottom: 30px;
	line-height: 2;
}

.product-remove {
    padding-left: 50px;
    border-bottom: 1px solid rgba(175, 175, 175, 0.5);
}

.product-subtotal {
    border-bottom: 1px solid rgba(175, 175, 175, 0.5);
}
.actions {
    padding-top: 50px;
}

.actions label {
    text-transform: uppercase;
    font-family: 'Lato-Regular';
    margin-right: 15px;
    color: #777;
}

.actions input {
    width: 52% !important;
    padding: 7px;
    margin-right: -3px;
    font-family: 'Lato-Regular', sans-serif;
    box-shadow: unset;
    border: 2px solid #e5e5e5;
}

.actions button {
    border: none;
    vertical-align: unset;
    padding: 9px 12px;
    margin: unset;
    line-height: unset;
}

.coupon {
    margin-bottom: 20px;
    width: 67%;
}

a.remove {
    font-size: 0;
}



.qty {
   text-align: center;
   text-decoration: none !important;
   font-size: 8pt;
   padding: unset;
   border: 1px solid #ddd;
}

input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button {
    opacity: 1;
}

tbody tr:nth-last-of-type(2) .product-remove {
	border-bottom: none;
}

.product-remove a img {
	max-width: 20px;
}

tbody tr:nth-last-of-type(2) .product-thumbnail {
	border-bottom: none;
}

tbody tr:nth-last-of-type(2) .product-name {
	border-bottom: none;
}

tbody tr:nth-last-of-type(2) .product-price {
	border-bottom: none;
}

tbody tr:nth-last-of-type(2) .product-quantity {
	border-bottom: none;
}

tbody tr:nth-last-of-type(2) .product-subtotal {
	border-bottom: none;
}

.woocommerce-error, .woocommerce-message {
    margin-left: auto !important;
    margin-right: auto !important;
    margin-bottom: 50px !important;
    width: 30%;
}

.woocommerce-cart-form, .cart-collaterals {
    margin: unset !important;
}



/* --- mobile --- */

@media (min-width: 0px) and (max-width: 770px) {

    .entry-title {
		text-align: center;
    }
    
    table.shop_table_responsive tr td {
        margin: auto;
        width: 90%;
        line-height: 3;
    }

    .shop_table {
    	width: 100%;
    }

    tbody tr:nth-last-of-type(2) .product-remove {
		border-bottom: 1px solid rgba(175, 175, 175, 0.5) ;
    }

    tbody tr:nth-last-of-type(2) .product-thumbnail {
        border-bottom: 1px solid rgba(175, 175, 175, 0.5);
    }

    tbody tr:nth-last-of-type(2) .product-name {
        border-bottom: 1px solid rgba(175, 175, 175, 0.5); 
    }

    tbody tr:nth-last-of-type(2) .product-price {
        border-bottom: 1px solid rgba(175, 175, 175, 0.5);
    }

    tbody tr:nth-last-of-type(2) .product-quantity {
        border-bottom: 1px solid rgba(175, 175, 175, 0.5);
    }

    tbody tr:nth-last-of-type(2) .product-subtotal {
        border-bottom: 1px solid rgba(175, 175, 175, 0.5);
        margin-bottom: 15px !important;
    }

    table.shop_table_responsive tr td:before {
		content: attr(data-title) "" !important;
    }
    
    a.remove:before {
		margin: auto;
		background-position-y: 0px;
		background-size: 25px;
		width: 100%;
		height: 26px;
		margin-top: 20px;
    	margin-bottom: -20px;
	}

	.product-remove {
		padding-left: unset !important;
    }
    
    .product-thumbnail {
        padding-right: unset;
        text-align: center !important;
    }
    
    .product-name, .product-price, .product-quantity, .product-subtotal {
        line-height: 3;
    }

    .product-name {
    	padding-right: unset;
    }

    .qty {
    	width: 35px;
    	height: 20px
    }

	.cart-collaterals {
  		border-top: 1px solid rgba(175, 175, 175, 0.5);
  		border-left: unset;
  		width: 90%;
  		margin: auto !important;
        position: unset;
        margin-top: 60px !important;
        padding-top: 45px;
    }
    
    .cart_totals {
        width: 255px;
        margin: auto !important;
        text-align: center;
        margin-left: -15px;
    }

    .actions input {
    	margin-right: unset;
    	width: 100% !important;
    	margin-right: unset;
    	padding: 15px;
    }

    .coupon {
        text-align: center;
        width: 100%;
    }

	.button {
		margin-left: auto;
        margin-right: auto;
        padding: 4px !important;
        margin-bottom: 70px;
        width: 100%;
    }
    
    .woocommerce {
        margin-bottom: unset;
    }

    .product-subtotal {
        margin-bottom: 50px !important;
    }

    .actions label {
        margin-right: unset;
    }
}

/* --- breakpoints --- */

@media (min-width: 770px) and (max-width: 1060px) {
    .actions input {
		width: 80% !important;
	}

	.actions button {
		margin-left: 112px;
	}

	.cart-collaterals {
        bottom: -20%;
        right: 3%;
    }
    
    .woocommerce {
	    padding: 0 50px;
	}

	.entry-title {
	    padding-left: 50px;
	}
}

@media (min-width: 770px) and (max-width: 918px) {
    .actions button {
        margin-left: unset;
    }

    .cart-collaterals {
        right: 5%;
    }
}
</style>


<form class="woocommerce-cart-form" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
	<?php do_action( 'woocommerce_before_cart_table' ); ?>

	<table class="shop_table shop_table_responsive cart woocommerce-cart-form__contents" cellspacing="0">
		<thead>
			<tr>
				<th class="product-remove">&nbsp;</th>
				<th class="product-thumbnail">&nbsp;</th>
				<th class="product-name"><?php esc_html_e( 'Product', 'woocommerce' ); ?></th>
				<th class="product-price"><?php esc_html_e( 'Price', 'woocommerce' ); ?></th>
				<th class="product-quantity"><?php esc_html_e( 'Quantity', 'woocommerce' ); ?></th>
				<th class="product-subtotal"><?php esc_html_e( 'Total', 'woocommerce' ); ?></th>
			</tr>
		</thead>
		<tbody>
			<?php do_action( 'woocommerce_before_cart_contents' ); ?>

			<?php
			foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
				$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
				$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
				if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
					$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
					?>
					<tr class="woocommerce-cart-form__cart-item <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">

						<td class="product-remove">
							<?php
								// @codingStandardsIgnoreLine
								echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
									'<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s">&times; <img class="skip-lazy" src="https://capranea.com/finalstaging/wp-content/themes/capranea/assets/cart/trash_solo.png"></a>',
									esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
									__( 'Remove this item', 'woocommerce' ),
									esc_attr( $product_id ),
									esc_attr( $_product->get_sku() )
								), $cart_item_key );
							?>
						</td>

						<td class="product-thumbnail">
						<?php
						$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
						if ( ! $product_permalink ) {
							echo wp_kses_post( $thumbnail );
						} else {
							printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), wp_kses_post( $thumbnail ) );
						}
						?>
						</td>

						<td class="product-name" data-title="<?php esc_attr_e( 'Product', 'woocommerce' ); ?>">
						<?php
						if ( ! $product_permalink ) {
							echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;' );
						} else {
							echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_name() ), $cart_item, $cart_item_key ) );
						}
						do_action( 'woocommerce_after_cart_item_name', $cart_item, $cart_item_key );
						// Meta data.
						echo wc_get_formatted_cart_item_data( $cart_item ); // PHPCS: XSS ok.
						// Backorder notification.
						if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
							echo wp_kses_post( apply_filters( 'woocommerce_cart_item_backorder_notification', '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>' ) );
						}
						?>
						</td>

						<td class="product-price" data-title="<?php esc_attr_e( 'Price', 'woocommerce' ); ?>">
							<?php
								echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.
							?>
						</td>

						<td class="product-quantity" data-title="<?php esc_attr_e( 'Quantity', 'woocommerce' ); ?>">
						<?php
						if ( $_product->is_sold_individually() ) {
							$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
						} else {
							$product_quantity = woocommerce_quantity_input( array(
								'input_name'   => "cart[{$cart_item_key}][qty]",
								'input_value'  => $cart_item['quantity'],
								'max_value'    => $_product->get_max_purchase_quantity(),
								'min_value'    => '0',
								'product_name' => $_product->get_name(),
							), $_product, false );
						}
						echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item ); // PHPCS: XSS ok.
						?>
						</td>

						<td class="product-subtotal" data-title="<?php esc_attr_e( 'Total', 'woocommerce' ); ?>">
							<?php
								echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.
							?>
						</td>
					</tr>
					<?php
				}
			}
			?>

			<?php do_action( 'woocommerce_cart_contents' ); ?>

			<tr>
				<td colspan="6" class="actions">

					<?php if ( wc_coupons_enabled() ) { ?>
						<div class="coupon">
							<label for="coupon_code"><?php esc_html_e( 'Coupon:', 'woocommerce' ); ?></label> <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>" /> <button type="submit" class="button" name="apply_coupon" value="<?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?>"><?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?></button>
							<?php do_action( 'woocommerce_cart_coupon' ); ?>
						</div>
					<?php } ?>

					<button type="submit" class="button" name="update_cart" value="<?php esc_attr_e( 'Update cart', 'woocommerce' ); ?>"><?php esc_html_e( 'Update cart', 'woocommerce' ); ?></button>

					<?php do_action( 'woocommerce_cart_actions' ); ?>

					<?php wp_nonce_field( 'woocommerce-cart', 'woocommerce-cart-nonce' ); ?>
				</td>
			</tr>

			<?php do_action( 'woocommerce_after_cart_contents' ); ?>
		</tbody>
	</table>
	<?php do_action( 'woocommerce_after_cart_table' ); ?>
</form>

<div class="cart-collaterals">
	<?php
		/**
		 * Cart collaterals hook.
		 *
		 * @hooked woocommerce_cross_sell_display
		 * @hooked woocommerce_cart_totals - 10
		 */

		do_action( 'woocommerce_cart_collaterals' );
	?>
</div>

<?php do_action( 'woocommerce_after_cart' ); ?>