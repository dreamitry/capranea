<style>

.post-69 {
    margin-top: 50px;
    margin-bottom: 70px;
}

.entry-header h1 {
    text-transform: uppercase;
    font-size: 12pt;
    letter-spacing: 5.5pt;
    font-family: 'Garogier', serif;
}

.woocommerce-billing-fields h3 {
    margin-top: 20px;
    margin-bottom: 20px;
}

#order_review_heading {
    display: none;
}

.checkout, .woocommerce-checkout {
    display: flex;
    justify-content: space-between;
    flex-wrap: wrap;
}

.woocommerce-checkout-review-order { 
    width: 50%;
    padding-top: 60px;
}

.col2-set {
    width: 39.7%;
}

.col2-set .form-row-first, .col2-set .form-row-last {
    width: unset !important;
}

.form-row {
    display: flex;
    flex-direction: column;
    margin: 0 14px 15px 0 !important;
}

.form-row label {
    font-family: 'Lato-Light', sans-serif;
    margin-bottom: 5px;
}

.form-row input {
    padding: 5px 10px;
    font-family: 'Lato-Regular', sans-serif;
}

.form-row-last {
    float: unset !important;
}

#order_comments_field .input-text {
    width: 100% !important;
    height: 100px;
}

#billing_company_field {
    float: left;
}

#billing_city_field {
    float: left;
}

#billing_address_1, #billing_address_2 {
    width: 100% !important;
}

#billing_address_1_field {
    clear: both;
}

#billing_phone_field {
    clear: both;
    float: left;
}

.woocommerce-account-fields {
    clear: both;
}

.select2-selection--single {
	width: 200px !important;
    border: 1px solid #e5e5e5 !important;
    font-size: 10pt;
    font-family: 'Lato-Regular', sans-serif;
    border-radius: unset !important;
}

#billing_country option {
    width: 200px;
    font-family: 'Lato-Regular';
    font-size: 10pt;
}

.input-text {
    width: 200px !important;
    border: 1px solid #e5e5e5;
}

#billing_city input {
    width: 100%;
}

.shop_table, .woocommerce-checkout-review-order-table {
	width: 100%;
	background-color: #f4f4f4;
	padding: 20px 15px;
}

.product-name {
	text-align: left;
}

.cart-subtotal td, .product-total, .order-total td {
	text-align: right;
}

#order_review tfoot {
    text-align: left;
}

thead .product-name, thead .product-total {
    border-bottom: 1px solid rgba(175, 175, 175, 0.5);
    line-height: 2;
}

.cart_item {
	line-height: 2;
}

.cart-subtotal {
	line-height: 2;
}

#payment {
	margin-top: 20px;
}

#stripe-payment-data p {
	font-size: 10pt;
    margin-top: 5px;
    margin-bottom: 10px;
    font-family: 'Lato-Light', sans-serif;
}

.wc_payment_method label, .payment_method_stripe label {
	line-height: 1.9;
}

.wc_payment_method .payment_box label {
    padding-left: 7px;
}

.woocommerce-SavedPaymentMethods-new {
    margin-bottom: 15px;
}

.woocommerce-terms-and-conditions-wrapper {
    font-size: 10pt;
    font-family: 'Lato-Light';
    margin-bottom: 25px;
}

.place-order button {
    width: 100%;
}

.order {
    letter-spacing: 1pt;
    font-family: 'Lato-Regular', sans-serif;
    color: #444;
}

.order strong {
    margin-left: 5px;
    font-family: 'Lato-Regular', sans-serif;
    color: #000;
    /*font-weight: normal;*/
}

.wc-stripe-checkout-button {
    margin-left: unset !important;
}

.woocommerce-additional-fields {
    margin-top: 30px;
}

.woocommerce-additional-fields h3, .woocommerce-additional-fields label {
    margin-bottom: 5px;
}

.woocommerce-NoticeGroup-checkout {
	display: flex; 
    flex-direction: column;
    width: 100% !important;
}

/* --- mobile --- */

@media (min-width: 0px) and (max-width: 930px) {
    .checkout, .woocommerce-checkout {
        flex-direction: column;
    }

    .col2-set {
        width: 85%;
        margin: auto;
    }

    .woocommerce-checkout-review-order {
    	width: 85%;
    	margin: auto;
    }

    .form-row, input[type=text], select, .select2-selection--single {
        width: 100% !important;
    }

    .entry-header h1 {
        padding-left: 65px;
    }
}

@media (min-width: 930px) and (max-width: 1100px) {
    .entry-content {
        padding: 0 35px;
    }

    .entry-header h1 {
        padding-left: 65px;
    }
}

</style>



<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
wc_print_notices();
do_action( 'woocommerce_before_checkout_form', $checkout );
// If checkout registration is disabled and not logged in, the user cannot checkout
if ( ! $checkout->is_registration_enabled() && $checkout->is_registration_required() && ! is_user_logged_in() ) {
	echo apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) );
	return;
}
?>

<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">

	<?php if ( $checkout->get_checkout_fields() ) : ?>

		<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

		<div class="col2-set" id="customer_details">
			<div class="col-1">
				<?php do_action( 'woocommerce_checkout_billing' ); ?>
			</div>

			<div class="col-2">
				<?php do_action( 'woocommerce_checkout_shipping' ); ?>
			</div>
		</div>

		<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

	<?php endif; ?>

	<h3 id="order_review_heading"><?php _e( 'Your order', 'woocommerce' ); ?></h3>

	<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

	<div id="order_review" class="woocommerce-checkout-review-order">
		<?php do_action( 'woocommerce_checkout_order_review' ); ?>
	</div>

	<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>

</form>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>