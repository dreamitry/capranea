<?php
/**
 * Single Product tabs
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/tabs.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.4.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
/**
 * Filter tabs and allow third parties to add their own.
 *
 * Each tab is an array containing title, callback and priority.
 * @see woocommerce_default_product_tabs()
 */
$tabs = apply_filters( 'woocommerce_product_tabs', array() );
if ( ! empty( $tabs ) ) : ?>

	<div class="more-product-content">
        <div class="more-product-content-inner">
			<div class="product-materials">
            <?php foreach ( $tabs as $key => $tab ) : ?>
            <div class="product-materials-content">
                    <a class="link-tab-descrion" "><h4><?php echo apply_filters( 'woocommerce_product_' . $key . '_tab_title', esc_html( $tab['title'] ), $key ); ?></h4></a>
                    <span><img src="https://mestech.co/wp-content/themes/capranea/assets/images/link-arrow-999.svg"></span>
                    <div class="tabs woocommerce-Tabs-panel--<?php echo esc_attr( $key ); ?> panel entry-content wc-tab" id="tab-<?php echo esc_attr( $key ); ?>" role="tabpanel" aria-labelledby="tab-title-<?php echo esc_attr( $key ); ?>">
				<?php if ( isset( $tab['callback'] ) ) { call_user_func( $tab['callback'], $key, $tab ); } ?>
			</div>
            </div>
            <?php endforeach; ?>

                <?php 

                    $futterung = get_field('futterung');  
                    $isolierung = get_field('isolierung'); 
                    $material = get_field('material');

                    if(!empty($futterung)) {
                        echo '     <div class="product-materials-content">
                        <a class="link-tab-descrion" ><h4>';
                        $blog_id3 = get_current_blog_id();
                        if ($blog_id3 === 2 or $blog_id3 === 3 ) {
                            echo 'Fitting';  } else {
                                
                                echo 'Futterung' ;
                            }
                        echo '</h4></a>
                        <span><img src="https://mestech.co/wp-content/themes/capranea/assets/images/link-arrow-999.svg"></span>
                        <div class="tabs woocommerce-Tabs-panel panel entry-content wc-tab" id="tab-fitterung" role="tabpanel" aria-labelledby="tab-title">';
                        echo $futterung;
                        echo '</div> </div>';
                    }
                    
                    if(!empty($isolierung)) {
                        echo '     <div class="product-materials-content">
                        <a class="link-tab-descrion" ><h4>';
                        $blog_id3 = get_current_blog_id();
                        if ($blog_id3 === 2 or $blog_id3 === 3 ) {
                            echo 'Isolation';  } else {
                                
                                echo 'ISOLIERUNG' ;
                            }
                            echo '</h4></a>
                        <span><img src="https://mestech.co/wp-content/themes/capranea/assets/images/link-arrow-999.svg"></span>
                        <div class="tabs woocommerce-Tabs-panel panel entry-content wc-tab" id="tab-fitterung" role="tabpanel" aria-labelledby="tab-title">';
                        echo $isolierung;
                        echo '</div> </div>';
                    }


                    if(!empty($material)) {
                        echo '     <div class="product-materials-content">
                        <a class="link-tab-descrion" ><h4>MATERIAL</h4></a>
                        <span><img src="https://mestech.co/wp-content/themes/capranea/assets/images/link-arrow-999.svg"></span>
                        <div class="tabs woocommerce-Tabs-panel panel entry-content wc-tab" id="tab-fitterung" role="tabpanel" aria-labelledby="tab-title">';
                        echo $material;
                        echo '</div> </div>';
                    }




                ?>

            </div>
           
        </div>

     <div class="more-product-content-inner">
			<div class="product-materials">
                <div class="product-materials-content product-materials-content-care">
               <?php 
                $blog_id3 = get_current_blog_id(); if ($blog_id3 === 2 or $blog_id3 === 3 ) {
                            echo ' <a class="link-tab-descrion" target="_blank" href="https://capranea.com/finalstaging/en/care-instructions/" ><h4 style="    text-align: center;">CARE INSTRUCTIONS</h4></a>'; 
                        
                        } else {
                                
                                echo ' <a class="link-tab-descrion" target="_blank" href="https://capranea.com/finalstaging/waschanleitung/" ><h4 style="    text-align: center;">PFLEGE UND WASCHHINWEISE</h4></a>';
                            }
                            
                            ?>
                                    
                                
                        

                </div>
            </div>
    </div>

	
</div>

<?php endif; ?>    