<?php 

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
if ( $related_products ) : ?>

	<section class="goes-well-with">

		<h3><?php esc_html_e( 'DAZU PASST', 'woocommerce' ); ?></h3>

		<?php woocommerce_product_loop_start(); ?>

			<?php foreach ( $related_products as $related_product ) : ?>

				<?php
				 	$post_object = get_post( $related_product->get_id() );
					setup_postdata( $GLOBALS['post'] =& $post_object );
					wc_get_template_part( 'content', 'product' ); ?>

			<?php endforeach; ?>

		<?php woocommerce_product_loop_end(); ?>

	</section>

<?php endif;
wp_reset_postdata();