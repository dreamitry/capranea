<?php /* Template Name: About Us */




get_header();



?> 


<div class="care-instructions-header">

    <div class="care-instructions-header-overlay">
               <h1>
               <?php 
               $blog_id3 = get_current_blog_id();
               if ($blog_id3 === 2 or $blog_id3 === 3 ) {
                            echo ' REFINED SKIWEAR';  } else {
                                
                                echo 'EINE SCHWEIZER ENTWICKLUNG, MADE IN EUROPE ' ;
                            }?> </h1>

              

    </div>

    
</div>


<div class="care-instructions-text-container"> 

    <div class="content-page-header">
        <h2>CAPRANEA<h2>
    </div>

    <div class="first-row row"> 
        <div class="first-column column">

        <?php 
               $blog_id3 = get_current_blog_id();
               if ($blog_id3 === 2 or $blog_id3 === 3 ) {
                            echo '<h1>REFINED SKIWEAR</h1>
                            <p>
                            Feel the function without seeing it - that is the basis on which Capranea redefines skiwear. "Urban lifestyle" is combined with technically functional materials. Many of these fabrics and products are being used for the first time in their field; an expression of the persistent research drive of Capranea. </p>
                 
                            <p>The harmoniously incorporated use of leather or tweed emphasizes its high quality and lends Capranea a characteristic look. An exciting interplay of contours and structure, nuanced by color input somewhere between restrained urban and strikingly sporty.

                            </p>';  } else {
                                
                                echo '<h1>REFINED SKIWEAR</h1>
                                <p>
                                Funktion fühlen, aber sie nicht sehen - auf dieser Basis definiert Capranea Skisportbekleidung neu. „Urban Lifestyle“ wird mit technisch funktionellen Materialien vereint. Viele dieser Stoffe und Fabrikate gelangen zum ersten Mal in der Branche zum Einsatz, als Ausdruck des beharrlichen Forschungstriebs von Capranea.</p>
                     
                                <p>Harmonisch eingearbeitete Einsätze aus Leder oder Tweedwolle unterstreichen die Hochwertigkeit und verleihen Capranea eine charakteristische Optik: ein spannendes Spiel mit Kontur und Struktur, nuanciert vom Farbeinsatz zwischen dezent urban und auffallend sportlich.</p>' ;
                            }?> 

            
            
        </div>

        <div class="second-column column">
                 <img src="https://capranea.com/uploaded/story_image1.jpg">
            
           
        </div>

    </div>
    <div class="second-row-container img-row-container"> 
        <div class="dark-overlay-row-container">

            <div class="second-row row">
                <div class="first-column column about-us-row-filler">
                
                </div>

                <div class="second-column column">

                <?php 
               $blog_id3 = get_current_blog_id();
               if ($blog_id3 === 2 or $blog_id3 === 3 ) {
                            echo '   <h2> A SWISS DEVELOPMENT, MADE IN EUROPE
                            </h2>
                            <p>All Capranea models originate in Switzerland. From the design to the digital cutting data through to the finished prototype, Capranea develops its products here. The entire material acquisition is also fully controlled from Switzerland. A relationship based on partnership with its 20 or so suppliers, enabling special developments and modifications of a good 40 components. This ultimately results in a unique product made of 300 constituent parts. 
                            </p>
        
                            <p>The production is largely carried out by hand in Portugal. The geographical as well as cultural proximity to our producers is a major advantage for development, reaction rate and not least quality. Our largest partner, the company PETRATEX is one of the best and most innovative manufacturers of functional textiles in Europe. Thanks to our development and the intensive and close cooperation with <a href="http://www.petratex.com/" target="_blank">PETRATEX</a>, over time we were able to take our products to an unprecedented Level.

                            </p>
                       ';  } else {
                                
                                echo '   <h2> EINE SCHWEIZER ENTWICKLUNG, MADE IN EUROPE</h2>
                                <p>Alle Capranea Modelle haben ihren Ursprung in der Schweiz. Von der Zeichnung über die digitalen Schnittdaten bis hin zum fertigen Prototyp entwickelt Capranea seine Produkte hier. Die gesamte Materialbeschaffung wird ebenfalls vollumfänglich aus der Schweiz gesteuert. Zu den rund 20 Lieferanten besteht eine partnerschaftliche Beziehung, was spezielle Entwicklungen und Modifizierungen der gut 40 Komponenten ermöglicht. So entsteht schlussendlich ein einzigartiges Produkt aus 300 Einzelteilen.  </p>
            
                                <p>Die Herstellung erfolgt zum grössten Teil in Portugal von Hand. Die räumliche wie auch kulturelle Nähe zu unseren Produzenten ist ein zentraler Vorteil in Bezug auf die Entwicklung, Reaktionsgeschwindigkeit und nicht zuletzt auf die Qualität. Unser grösster Partner, die Firma <a href="http://www.petratex.com/">PETRATEX</a> gilt als einer der besten und innovativsten Hersteller von funktionellen Textilien in Europa. Dank unserer Entwicklung und der intensiven und engen Zusammenarbeit mit PETRATEX, konnten wir über die Jahre unsere Produkte auf ein Level bringen, welches seinesgleichen sucht.</p>
                           ' ;
                            }?> 
                  </div>
            </div>
        </div>
    </div>
    <div class="third-row-container">
        <div class="second-row row">
            <div class="first-column column">
            <?php 
               $blog_id3 = get_current_blog_id();
               if ($blog_id3 === 2 or $blog_id3 === 3 ) {
                            echo '             
                            <h2>LEAVE YOUR MARK</h2>
                            <p>Traces are reminders of what we have experienced. We pursue our passion with confidence and leave traces in surroundings in which sustainability is of prime importance. We structure and develop our vision in harmony with sport and nature, as an expression of our conviction. What we do shows who we are.

                            </p>
                            ';  } else {
                                
                                echo '            
                                <h2>LEAVE YOUR MARK</h2>
                                <p>Spuren sind Erinnerungen unserer Erlebnisse. Selbstbewusst folgen wir unserer Leidenschaft. Wir hinterlassen Spuren in einer Umgebung, in der Nachhaltigkeit von grosser Bedeutung ist. In Harmonie mit Sport und Natur gestalten und entwickeln wir unsere Vision, als Ausdruck unserer Überzeugung. Was wir tun, zeigt wer wir sind.</p>
                                ' ;
                            }?> 
              
              
            </div>

            <div class="second-column column">
             

                <img src="https://capranea.com/uploaded/story_image3.jpg">
        </div>
    </div>
    <div class="fourth-row-container grey-row-container">
        <div class="second-row row  ">
            <div class="first-column column">
                <img src="https://capranea.com/uploaded/1444034388-8982.jpg">
               
            </div>

            <div class="second-column column">
            <?php 
               $blog_id3 = get_current_blog_id();
               if ($blog_id3 === 2 or $blog_id3 === 3 ) {
                            echo '   <h2>HOME OF CAPRANEA</h2>
                            <p>Capranea is a product from the mountains of Grau­bünden where the ibex (lat. Capra ibex) lives. He reigns supreme across the forest border and treads his own path. Light-footed, agile and self-confident, he is found in small herds and is constantly on the move. Anyone who comes across him is fascinated by his elegance and Beauty.

                            ';  } else {
                                
                                echo '   <h2>HOME OF CAPRANEA</h2>
                                <p>Capranea ist ein Produkt aus den Bündner Bergen, dort wo der Steinbock (lat. Capra Ibex) heimisch ist. Über der Waldgrenze thront er und geht seinen eigenen Weg. Leichtfüssig und geschickt, selbstbewusst und nur in kleinen Rudeln ist er stets in Bewegung. Wer ihn entdeckt, ist fasziniert von der Eleganz und Grazie.</p> </div>
                        ' ;
                            }?>
             </div>
    </div>
  
</div>



<?php
get_footer();
