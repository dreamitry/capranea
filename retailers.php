<?php /* Template Name: Store Finder */




get_header();

$country = sanitize_text_field( $_GET['country']);


?> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.3.4/leaflet.js"></script>

<style>

 
 .form input[type="text"].input-md, .form input[type="email"].input-md, .form input[type="number"].input-md, .form input[type="url"].input-md, .form input[type="search"].input-md, .form input[type="tel"].input-md, .form input[type="password"].input-md, .form input[type="date"].input-md, .form input[type="color"].input-md, .form select.input-md {
    height: 37px;
    padding-left: 13px;
    padding-right: 13px;
    font-size: 12px;
    -webkit-border-radius: 0 !important;
    -moz-border-radius: 0 !important;
    border-radius: 0 !important;
}
.form select {
    font-size: 12px;
}
.form input[type="text"], .form input[type="email"], .form input[type="number"], .form input[type="url"], .form input[type="search"], .form input[type="tel"], .form input[type="password"], .form input[type="date"], .form input[type="color"], .form select {
    display: inline-block;
    height: 27px;
    vertical-align: middle;
    font-size: 11px;
    font-weight: 400;
    letter-spacing: 1px;
    color: #777;
    border: 1px solid rgba(0,0,0, .1);
    padding-left: 7px;
    padding-right: 7px;
    -webkit-border-radius: 0;
    -moz-border-radius: 0;
    border-radius: 0;
    -webkit-box-shadow: none;
    -moz-box-shadow: none;
    box-shadow: none;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    -webkit-transition: all 0.2s cubic-bezier(0.000, 0.000, 0.580, 1.000);
    -moz-transition: all 0.2s cubic-bezier(0.000, 0.000, 0.580, 1.000);
    -o-transition: all 0.2s cubic-bezier(0.000, 0.000, 0.580, 1.000);
    -ms-transition: all 0.2s cubic-bezier(0.000, 0.000, 0.580, 1.000);
    transition: all 0.2s cubic-bezier(0.000, 0.000, 0.580, 1.000);
}

.form-control {
    display: block;
    width: 100%;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,0.075);
    box-shadow: inset 0 1px 1px rgba(0,0,0,0.075);
    -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
}
input, button, select, textarea {
    font-family: inherit;
    font-size: inherit;
    line-height: inherit;
}
button, select {
    text-transform: none;
}
button, input, optgroup, select, textarea {
    color: inherit;
    font: inherit;
    margin: 0;
}


@media (max-width: 800px) {
    .content-page-header {
    padding-top: 110px;

    }
}

</style>






     <div class="content-page-header">
            <h2>STOREFINDER<h2>
        </div>

<!-- <div id='map' class="map" style='width: 100vw; height: 300px; margin: 0px;'></div> -->

    <div class="storefinder-container">
   
        <form class="form select-country" action="<?php echo get_site_url(); ?>/storefinder/" method="get">
            <select name="country" onchange="this.form.submit()">
               <option value="CH" <?php if ($country === 'CH') {
                    echo 'selected';
                } ?>>Switzerland</option>
                <option value="AD" <?php if ($country === 'AD') {
                    echo 'selected';
                } ?>>Andorra</option>
                <option value="AT" <?php if ($country === 'AT') {
                    echo 'selected';
                } ?>>Austria</option>
                <option value="BE" <?php if ($country === 'BE') {
                    echo 'selected';
                } ?>>Belgium</option>
                <option value="CA" <?php if ($country === 'CA') {
                    echo 'selected';
                } ?>>Canada</option>
                <option value="CZ" <?php if ($country === 'CZ') {
                    echo 'selected';
                } ?>>Czech Republic</option>
                <option value="NO" <?php if ($country === 'NO') {
                    echo 'selected';
                } ?>>Norway</option>
                <option value="FR" <?php if ($country === 'FR') {
                    echo 'selected';
                } ?>>France</option>
                <option value="DE" <?php if ($country === 'DE') {
                    echo 'selected';
                } ?>>Germany</option>
                <option value="IT" <?php if ($country === 'IT') {
                    echo 'selected';
                } ?>>Italy</option>
                <option value="NL" <?php if ($country === 'NL') {
                    echo 'selected';
                } ?>>Netherland</option>
                <option value="ES" <?php if ($country === 'ES') {
                    echo 'selected';
                } ?>>Spain</option>
                <option value="US" <?php if ($country === 'US') {
                    echo 'selected';
                } ?>>United States</option>
            </select>
        </form>
    </div>


  <div class="storefinder-stores">

<?php 

if ($blog_id != 1) {
    global $switched;
    switch_to_blog(1);
}

/* switching the loop to the main site for the lookbook to come from one source */  
?>


        <?php


            $country = $_GET["country"];

            $args = [

                'post_type'		=> 'stores',
                'meta_key'		=> 'storefinder-lkz',
                'numberposts'  => '50',
              

                
                
                


            ];

            if(!empty($country)) {
                $args['meta_value'] = $country;
            } else {
                $args['meta_value'] = 'CH';
            }


            $posts = get_posts($args);

           


            if( $posts ): ?>
	
                <div class="storefinder-content">
                    
                <?php foreach( $posts as $post ): 
                    
                    setup_postdata( $post );
                    
                    ?>
                  
                        <h2><?php the_title(); ?></h2>
                        <p><?php the_field('storefinder-strasse'); ?><br> <?php the_field('storefinder-plz'); ?> <?php the_field('storefinder-ort'); ?></p>
                       <?php $storefinderhomepage = get_field('storefinder-homepage'); 
                       
                       if ($storefinderhomepage) {

                            if(!(preg_match("@^http://@i" ,$storefinderhomepage) or preg_match("@^https://@i" ,$storefinderhomepage))) {
                                $storefinderhomepage = 'http://'.$storefinderhomepage;

                            };
                            echo '<a href="' . $storefinderhomepage . '" target="_blank">website</a>';

                       }
                        ?>
                        <hr>
                    
                
                <?php endforeach; ?>
                
                </div>
                
                <?php wp_reset_postdata(); ?>

            <?php endif; ?>
 
            
    </div>

    <?php restore_current_blog(); //switched back to main site (check above ?>
        
          

<script src='https://api.mapbox.com/mapbox-gl-js/v0.47.0/mapbox-gl.js'></script>
<link href='https://api.mapbox.com/mapbox-gl-js/v0.47.0/mapbox-gl.css' rel='stylesheet' />


<script>


mapboxgl.accessToken = 'pk.eyJ1IjoiZG90Z3JhZiIsImEiOiJjamxtbnF6YXQxNmVoM3FsNDZ3N3dlejFiIn0.R_25tO3fu8DAPduV2eLmng';
var map = new mapboxgl.Map({
container: 'map',
style: 'mapbox://styles/mapbox/dark-v9',
center: [5.38, 50], // starting position
zoom: 4 // starting zoom

});




map.addControl(new mapboxgl.NavigationControl());


var url = '<?php echo $countryurl; ?>';
map.on('load', function () {
    window.setInterval(function() {
        map.getSource('drone').setData(url);
    }, 2000);

    map.addSource('drone', { type: 'geojson', data: url });
    map.addLayer({
        "id": "drone",
        "type": "symbol",
        "source": "drone",
        "layout": {
            "icon-image": "rocket-15"
        }
    });
});


</script>


<?php
get_footer();
