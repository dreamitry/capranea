<?php /* Template Name: Care Instructions */




get_header();



?> 


<div class="care-instructions-header">

    <div class="care-instructions-header-overlay">
               <h1>  <?php 
               $blog_id3 = get_current_blog_id();
               if ($blog_id3 === 2 or $blog_id3 === 3 ) {
                            echo 'CARE INSTRUCTIONS';  } else {
                                
                                echo 'PFLEGE UND WASCHHINWEISE' ;
                            }?></h1>
    </div>

    
</div>
<div class="care-instructions-text-container"> 
    <div class="content-page-header">
        <h2><?php 
               $blog_id3 = get_current_blog_id();
               if ($blog_id3 === 2 or $blog_id3 === 3 ) {
                            echo 'CARE INSTRUCTIONS';  } else {
                                
                                echo 'PFLEGE UND WASCHHINWEISE' ;
                            }?><h2>
    </div>

    <div class="first-row row"> 
        <div class="first-column column">
            <img src="https://capranea.com/uploaded/1450294236-8875.jpg">
        </div>

        <div class="second-column column">

        <?php 
               $blog_id3 = get_current_blog_id();
               if ($blog_id3 === 2 or $blog_id3 === 3 ) {
                            echo '<h1>CARE INSTRUCTIONS</h1>
                            <p> All materials and components have been carefully selected to ensure a high level of functionality and comfort. Capranea is not only highly functional sportswear but also classy alongside the piste. </p>
                            <br>
                            <p>The same applies to all functional textiles: the less they are washed, the longer their shape, feel and look remains good. Should they need to be cleaned, a few things should be noted.
                            </p>';  } else {
                                
                                echo '<h1> PFLEGE UND WASCHHINWEISE</h1>
                                <p> Alle Materialien und Komponente sind sorgfältig ausgewählt um ein Höchstmass an Funktionalität und Komfort zu gewährleisten. Capranea ist nicht nur top funktionale Sportbekleidung sondern auch stilvoll neben der Piste.</p>
                                <br>
                                <p>Für alle Funktionstextilien gilt – je weniger sie gewaschen werden, umso länger bleiben sie schön in Form, Haptik und Optik. Ist eine Reinigung dennoch nötig, gibt es ein paar Dinge zu beachten.</p>' ;
                            }?>
            
            
           
        </div>

    </div>
    <div class="second-row-container grey-row-container">
        <div class="second-row row ">
            <div class="first-column column">


            <?php 
               $blog_id3 = get_current_blog_id();
               if ($blog_id3 === 2 or $blog_id3 === 3 ) {
                            echo ' <h2>DOWN JACKETS</h2>
                            <p>Where possible, use a drum washing machine. When using a tub washing machine (with agitator), we recommend protecting the item by washing it in a washing net. 
                            </p>
            
                           <p> 1. Empty all pockets and close all zippers 
                           </p>
            
                           <p> 2. Wash separately at max. 30°C on a gentle cycle or a delicates or wool cycle with minimal spin. 
                           </p>
            
                           <p> 3. Use none or as little detergent as possible, no fabric softener or refiner and no bleaching agents. If detergent is used, we recommend a second rinse cycle to remove all detergent residues. 
                           </p>
            
                           <p> 4. After washing, remove from the washing machine immediately, shake out and dry the down garment on low heat (permanent press cycle) with two clean tennis balls. It may take upwards of 3 hours for down garments to dry fully, but a garment will not be at its full loft/warmth until it is. 
                           </p>
            
                            <p><i>Tip: Keep ski clothing hanging in a dry place.
                            </i></p>';  } else {
                                
                                echo ' <h2>SKI JACKETS, TROUSERS AND MIDLAYERS</h2>
                                <p>Wenn möglich nutzen Sie eine Trommelwaschmaschine. Bei Verwendung einer Bottichwaschmaschine (mit Rührarm / Agitator) empfehlen wir das Kleidungsstück zum Schutz in einem Wäschenetz zu waschen.</p>
                
                               <p> 1. Alle Taschen leeren und Reissverschlüsse schliessen</p>
                
                               <p> 2. Einzelwäsche bei max. 30°C im Schonprogramm bzw. Fein- oder Wollwaschprogramm mit minimaler Schwingung.</p>
                
                               <p> 3. Verwenden Sie kein oder möglichst wenig Waschmittel, keinen Weichspüler oder Gewebeveredler, keine Bleichmittel. Falls Waschmittel verwendet wurde, empfehlen wir einen zweiten Spülgang, um Waschmittelrückstände gründlich auszuwaschen.</p>
                
                               <p> 4. Nach der Wäsche unverzüglich aus der Maschine nehmen, ausschütteln und im Tumbler mit zwei sauberen Tennisbällen im Schontrockengang auf kleinster Hitze komplett austrocknen (dies kann bis 3 Stunden dauern), danach aufhängen und in Form bringen. So erlangt die Daune wieder ihre Spannkraft und optimale Wärmeisolation. </p>
                
                                <p><i>Tip: Skikleider hängend an einem trockenen Ort aufbewahren.</i></p>' ;
                             }?>
               
            </div>

            <div class="second-column column">

            <?php 
               $blog_id3 = get_current_blog_id();
               if ($blog_id3 === 2 or $blog_id3 === 3 ) {
                            echo ' <h2> SKI JACKETS, TROUSERS AND MIDLAYERS</h2>
                            <p>Where possible, use a drum washing machine. When using a tub washing machine (with agitator), we recommend protecting the item by washing it in a washing net. 
                            </p>
        
                            <p>Where possible, use a drum washing machine. When using a tub washing machine (with agitator), we recommend protecting the item by washing it in a washing net. 
                            </p>
        
                            <p>2. Wash separately at max. 30°C on a gentle cycle or a delicates or wool cycle with minimal spin. </p>
        
                            <p>3. Use none or as little detergent as possible, no fabric softener or refiner and no bleaching agents. If detergent is used, we recommend a second rinse cycle to remove all detergent residues. 
                            </p>
        
                            <p>4. After washing, remove from the washing machine immediately, shake out and leave to dry flat. 
                            </p>
        
                            <p><i>Tip: Keep ski clothing hanging in a dry place.

                            </i></p>';  } else {
                                
                                echo ' <h2>SKIJACKEN UND -HOSEN SOWIE MIDLAYER</h2>
                                <p>Wenn möglich nutzen Sie eine Trommelwaschmaschine. Bei Verwendung einer Bottichwaschmaschine (mit Rührarm / Agitator) empfehlen wir das Kleidungsstück zum Schutz in einem Wäschenetz zu waschen.</p>
            
                                <p>1. Alle Taschen leeren und Reissverschlüsse schliessen</p>
            
                                <p>2. Einzelwäsche bei max. 30°C im Schonprogramm bzw. Fein- oder Wollwaschprogramm mit minimaler Schwingung.</p>
            
                                <p>3. Verwenden Sie kein oder möglichst wenig Waschmittel, keinen Weichspüler oder Gewebeveredler, keine Bleichmittel. Falls Waschmittel verwendet wurde, empfehlen wir einen zweiten Spülgang, um Waschmittelrückstände gründlich auszuwaschen.</p>
            
                                <p>4. Nach der Wäsche unverzüglich aus der Maschine nehmen, ausschütteln und liegend trocknen lassen. </p>
            
                                <p><i>Tip: Skikleider hängend an einem trockenen Ort aufbewahren.</i></p>' ;
                            }?>
               
            </div>
        </div>
    </div>
  
</div>



<?php
get_footer();
