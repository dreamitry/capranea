
// Revolution Slider Init 

(function($){
    "use strict";
    
    $(document).ready(function(){
        $(".fullscreenbanner").revolution({
            dottedOverlay: "none",
            delay: 16000,
            startwidth: 1170,
            startheight: 600,
            navigationStyle: "square-old",
            
            parallax: "scroll",
            parallaxBgFreeze: "on",
            parallaxLevels: "twoxtwo",
            hideTimerBar:"on"
        });
        
        $(".fullscreenbanner-s").revolution({
            delay: 5000,
            startwidth: 1170,
            startheight: 500,
            hideThumbs: 10,
            fullWidth: "off",
            fullScreen: "on",
            fullScreenOffsetContainer: "",
            navigationStyle: "square-old",

            parallax: "scroll",
            parallaxBgFreeze: "on",
            parallaxLevels: "twoxtwo",
            hideTimerBar:"on"
        });
        
        
    });
})(jQuery);
