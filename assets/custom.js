// GLOBAL //
////////////////////////////////////////////////////////////////////////////
//var server = "http://www.capranea.com/staging/";
//var server = "https://"+window.location.hostname+"/staging/";
var server = "";
var products_cart;
var shipping_info;
var order_info;
var onlineshop_products_cart;
var onlineshop_shipping_info;
var onlineshop_order_info;
var onlineshop_invoice;
var onlineshop_data;

// TEST MOBILE //
////////////////////////////////////////////////////////////////////////////
var mobileTest;
if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
     mobileTest = true;
}
 else {
     mobileTest = false;
}

// GLOBAL //
////////////////////////////////////////////////////////////////////////////

//Pad Number
function padLeft(nr, n, str){
    return Array(n-String(nr).length+1).join(str||'0')+nr;
}

// CATALOG //
////////////////////////////////////////////////////////////////////////////

// CHANGE LIST GRID //

function changeGrid() {

    //VARS
    var grid = $(this).attr("id").replace("grid","");

    //BUTTON
    $(".b-grid-change").removeClass("active");
    $(this).addClass("active");

    //REMOVE
    $( ".works-holder" ).animate({ opacity: 0,  }, "fast", function() {
        
        $(".works-grid").removeClass("work-grid-1");
        $(".works-grid").removeClass("work-grid-4");
        $(".works-grid").removeClass("work-grid-6");
        $(".works-grid").addClass("work-grid-" + grid);

        $("#work-grid").isotope({
            itemSelector: '.mix',
            layoutMode: "fitRows"
        });

        $( ".works-holder" ).animate({ opacity: 1,  }, "normal");

        //Filters
        //$(".filter").removeClass("active");
        $(".ball").addClass("active");

    });

}

function limitInput () {
    var value = parseInt(this.value, 10);
    var max = parseInt(this.max, 10);
    var min = parseInt(this.min, 10);

    if (value > max) {
        this.value = max;
    } else if (value < min) {
            this.value = min
    }
};

function reloadCatalog(elm, url, lang, source) {

    //Get Catalog source
    var catsource = 'get-catalog.php';
    if(source == 'order-zone') catsource = 'get-catalog-order-zone.php';

    //Categories
    var categories = ["jacket","pants","midlayers","accessoires"];

    //Category
    var category = elm.attr("id").replace("cat","");
    $(".bfilter").removeClass("active");
    elm.addClass("active");

    //Empty
    $( ".works-holder" ).animate({ opacity: 0,  }, "fast", function() {
        $('#work-grid').empty();
        work_grid.isotope('destroy');
        $(".content-loader").show();
    });

    post_data = {
        'url': url,
        'category': category,
        'lang': lang,
        'source': source
    };
            
    $.post('includes/'+catsource, post_data, function(response){

        $("#work-grid").append(response);
        
        work_grid.imagesLoaded(function(){
            work_grid.isotope({
                itemSelector: '.mix',
                layoutMode: isotope_mode,
                filter: '.'+categories[category-1]
            });

            $(".content-loader").hide();
            $( ".works-holder" ).animate({ opacity: 1  }, "normal");

            //Tooltip
            $('.tooltipster').tooltipster({theme: 'tooltipster-noir'});

        });


    });

}

// INIT FUNCTIONS //

function catalogGridInit(html, url, lang, source) {

    //VAR 
    var html = html;

    //Tooltip
    $('.tooltipster').tooltipster({theme: 'tooltipster-noir'});

    //Buttons
    $(".b-grid-change").click(changeGrid);

    if(mobileTest) {
        $("#grid1").trigger('click');
        $("#grid1").hide();
        $("#grid4").hide();
        $("#grid6").hide();

    }

    //Buttons
    $(".bfilter").click(function(){ reloadCatalog($(this), url, lang, source); });

     //Hash
    if(window.location.hash) {
                        
        var q = window.location.hash.replace(/^#/, '');
        var qa = q.split('q');

        if(qa[0] != 1) reloadCatalog($("#cat"+qa[0]), url, lang, source);

        window.location.hash = '';
        history.pushState('', document.title, window.location.pathname);

    }
    
}

function productInit(source, product, currency, zone, url, country, userlogged, onlineshop, lang, info_stock, total_stock) {

    //Vars
    var stock_message = info_stock;
    if(stock_message == "WENIGER ALS 3 STK. VERFÜGBAR" || stock_message == "MEHR ALS 3 STK. VERFÜGBAR" || stock_message == "AUSVERKAUFT" || stock_message == "LESS THAN 3PCS AVAILABLE" || stock_message == "MORE THAN 3 PCS AVAILABLE" || stock_message == "SOLD OUT"  ) stock_message = "";

    var ready_message = "LESS THAN 3PCS AVAILABLE";
    var indays_message = "MORE THAN 3 PCS AVAILABLE";
    var soldout_message = "SOLD OUT";

    //Tooltip
    $('.tooltipster').tooltipster({theme: 'tooltipster-noir'});

    //Product
    var product_json = JSON.parse(product);
    product_json.data.stock_message = stock_message;
    product = JSON.stringify(product_json);

    //Zoom
    $('.product-img').magnify();

    //Check Availability
    $('#b-check').click(function(){
        $(".check-title").fadeIn();
        $(".check-section").fadeIn();

        var hash_offset = $("#check-availability").offset().top-100;
        $("html, body").animate({
            scrollTop: hash_offset
        });
    });

    //Size Button
    $(".prod-size a").click(function() {

       var size = $(this).attr("id").replace("size","");
       var route = "";
       $(".prod-size a").removeClass("active");
       $(this).addClass("active");
       $("#quantity").val("1");

       $(".stock-cart-info").hide();
       $(".product-qty-stock").show();
       $(".product-qty-stock").html("");

       if (typeof product != "undefined"){ 

            post_data = {
                'op': 'getStock',
                'style_number': product_json.data.style.style_code,
                'color_number': product_json.data.color.color_code,
                'size_number': size
            };
            
            $.post(server + 'server-side/services.php', post_data, function(response){

                if(response.errorMsg == "") {

                    $('.product-add-form').show();
                    //$("#quantity").attr("max", response.data.stock);

                    var total_stock_qty = parseInt(response.data.stock);
                    var total_stock_ch_qty = parseInt(response.data.stock_ch);
                    var total_stock_eu_qty = parseInt(response.data.stock_eu);
                    var less_message = "";
                    var more_message = "";
                    var soldout_message = "";
                    route = response.data.route;

                    if(lang == "de") {
                        less_message = "WENIGER ALS 3 STK. VERFÜGBAR";
                        more_message = "MEHR ALS 3 STK. VERFÜGBAR";
                        soldout_message = "AUSVERKAUFT";
                    } else {
                        less_message = "LESS THAN 3PCS AVAILABLE";
                        more_message = "MORE THAN 3 PCS AVAILABLE";
                        soldout_message = "SOLD OUT";
                    }

                    if(userlogged != "1") {    

                        if(source == "retailer")  {

                            if(currency != "EUR") {

                                total_stock_qty = total_stock_ch_qty;

                                if(total_stock_ch_qty < 3 && total_stock_ch_qty > 0 ) {
                                    $('.product-qty-stock').html('<span><i class="fa fa-lg fa-check-circle orange-stock"></i></span>&nbsp;&nbsp;&nbsp;'+less_message);
                                    stock_message = "";
                                    $('.product-storefinder').hide();
                                }
                                else if(total_stock_ch_qty >= 3) {
                                    $('.product-qty-stock').html('<span><i class="fa fa-lg fa-check-circle green-stock"></i></span>&nbsp;&nbsp;&nbsp;'+more_message);
                                    stock_message = "";
                                    $('.product-storefinder').hide();
                                }                        
                                else { 

                                    $('.product-qty-stock').html('<span><i class="fa fa-lg fa-check-circle red-stock"></i></span>&nbsp;&nbsp;&nbsp;'+soldout_message); 
                                    $('.product-add-form').hide();
                                    $('.product-storefinder').show();
                                }

                            } else {

                                total_stock_qty = total_stock_eu_qty;

                                if(total_stock_eu_qty < 3 && total_stock_eu_qty > 0 ) {
                                    $('.product-qty-stock').html('<span><i class="fa fa-lg fa-check-circle orange-stock"></i></span>&nbsp;&nbsp;&nbsp;'+less_message);
                                    stock_message = "";
                                    $('.product-storefinder').hide();
                                }
                                else if(total_stock_eu_qty >= 3) {
                                    $('.product-qty-stock').html('<span><i class="fa fa-lg fa-check-circle green-stock"></i></span>&nbsp;&nbsp;&nbsp;'+more_message);
                                    stock_message = "";
                                    $('.product-storefinder').hide();
                                }                        
                                else { 

                                    $('.product-qty-stock').html('<span><i class="fa fa-lg fa-check-circle red-stock"></i></span>&nbsp;&nbsp;&nbsp;'+soldout_message); 
                                    $('.product-add-form').hide();
                                    $('.product-storefinder').show();
                                }

                            }

                        } else {

                            if(total_stock_qty < 3 && total_stock_qty > 0 ) {
                                $('.product-qty-stock').html('<span><i class="fa fa-lg fa-check-circle orange-stock"></i></span>&nbsp;&nbsp;&nbsp;'+less_message);
                                stock_message = "";
                                $('.product-storefinder').hide();
                            }
                            else if(total_stock_qty >= 3) {
                                $('.product-qty-stock').html('<span><i class="fa fa-lg fa-check-circle green-stock"></i></span>&nbsp;&nbsp;&nbsp;'+more_message);
                                stock_message = "";
                                $('.product-storefinder').hide();
                            }                        
                            else { 

                                $('.product-qty-stock').html('<span><i class="fa fa-lg fa-check-circle red-stock"></i></span>&nbsp;&nbsp;&nbsp;'+soldout_message); 
                                $('.product-add-form').hide();
                                $('.product-storefinder').show();
                            }

                        }    

                    } else {


                        var total_stock_qty = parseInt(response.data.stock);
                        var stock_ch = parseInt(response.data.stock_ch);
                        var stock_eu = parseInt(response.data.stock_eu);

                        if(lang == "de") {
                            ready_message = "VERSANDBEREIT";
                            indays_message = "VERSANDBEREIT IN 10-15 TAGEN";
                            soldout_message = "AUSVERKAUFT";
                        } else {
                            ready_message = "READY FOR DELIVERY";
                            indays_message = "READY FOR DELIVERY IN 10-15 DAYS";
                            soldout_message = "SOLD OUT";
                        }

                        if(country == "CHE") {

                            if(stock_ch != 0 ) {
                                $('.product-qty-stock').html('<span><i class="fa fa-lg fa-check-circle green-stock"></i></span>&nbsp;&nbsp;&nbsp;'+ready_message);
                                stock_message = "READY FOR DELIVERY";
                                $('.product-storefinder').hide();
                            }
                            else if(stock_eu != 0) {
                                $('.product-qty-stock').html('<span><i class="fa fa-lg fa-check-circle green-stock"></i></span>&nbsp;&nbsp;&nbsp;'+indays_message);
                                stock_message = "READY FOR DELIVERY IN 10-15 DAYS";
                                $('.product-storefinder').hide();
                            }

                        } else {

                            if(stock_eu != 0 ) { 
                                $('.product-qty-stock').html('<span><i class="fa fa-lg fa-check-circle green-stock"></i></span>&nbsp;&nbsp;&nbsp;'+ready_message);
                                stock_message = "READY FOR DELIVERY";
                                $('.product-storefinder').hide();
                            }
                            else if(stock_ch != 0) {
                                $('.product-qty-stock').html('<span><i class="fa fa-lg fa-check-circle green-stock"></i></span>&nbsp;&nbsp;&nbsp;'+indays_message);
                                stock_message = "READY FOR DELIVERY IN 10-15 DAYS";
                                $('.product-storefinder').hide();
                            }

                        }

                        if(onlineshop != "Y" || total_stock_qty == 0 || isNaN(total_stock)){ 
                            
                            $('.product-qty-stock').html('<span><i class="fa fa-lg fa-check-circle red-stock"></i></span>&nbsp;&nbsp;&nbsp;'+soldout_message); 
                            $('.product-add-form').hide();
                            $('.product-storefinder').show();

                        } 


                    }

                    total_stock = total_stock_qty;
                    product_json.data.size_number = size;
                    product_json.data.stock_message = stock_message;
                    if(route != "" ) product_json.data.route = route;
                    product = JSON.stringify(product_json);

                }

            }, 'json');


       } else {

            $("#product").val($(".prod-title").html() + ", " + $(".color-active").attr("id") + ", " + size);
            $('input[name=product]').css('border-color', '');
           
            var product_code = $("#productcode").val();
            product_code = product_code.slice(0,-2);
            product_code = product_code + size;
            $("#productcode").val(product_code);

       }

       return false;

    });

    //Reversible
    var image = $(".product-image img").attr("data-src");
    var imageBig = $(".product-image img").attr("data-magnify-src");

    $("#b-reversible").click(function() {

        var imagerev = $(this).attr("data-src");
        var imagerevBig = $(this).attr("data-magnify-src");

        if($(".product-image img").attr("src") != imagerev) {

            $(".product-image img").attr("src", imagerev);
            $(".product-image img").attr("data-magnify-src", imagerevBig);
            $('.product-img').magnify();
            $('#b-download').attr("href", imagerevBig);
        
        } else {

            $(".product-image img").attr("src", image);
            $(".product-image img").attr("data-magnify-src", imageBig);
            $('.product-img').magnify();
            $('#b-download').attr("href", imageBig);

        }

    });

    //Add Product to Cart
    if (typeof product != "undefined"){ 

        //Quantity Check
        //$("#quantity").change(limitInput);

        //Add to Cart
        $("#b-add-cart").click(function() {

            if(total_stock > 0) {

                if($("#quantity").val() > 0) {

                    //Hide Quantity Warning
                    $(".onlineshop-add-result").hide();

                    //Create Object
                    var product_json = JSON.parse(product);
                    var object = new Object();
                    var rcode = product_json.data.label.replace(/\./g,'')
                    var d = new Date();
                    var n = d.getTime();

                    object.id = product_json.data.id;
                    object.name = product_json.data.category.title + " " + product_json.data.style.title;
                    object.art_num = product_json.data.style.style_code;
                    object.size_num = product_json.data.size_number;
                    object.color = product_json.data.color.title;
                    object.color_code = product_json.data.color.color_code;
                    object.url = url;
                    object.route = product_json.data.route;
                    object.qty = $("#quantity").val();
                    object.image = product_json.data.images.product;
                    object.rcode = "R"+rcode+"-"+n;
                    object.stock_message = product_json.data.stock_message;

                    if(source == "retailer") {

                        if(currency == "EUR") {

                            object.price_eur_retailer = product_json.data.price_eur_retailer;
                            object.price_total = Number(object.price_eur_retailer) * Number($("#quantity").val());
                            object.currency = "EUR";
                            
                        } else {

                            object.price_chf_retailer = product_json.data.price_chf_retailer;
                            object.price_total = Number(object.price_chf_retailer) * Number($("#quantity").val());
                            
                            if(currency == "USD") 
                                object.currency = "USD";
                            else 
                                object.currency = "CHF";
                            

                        }

                        addProductToCart(object, total_stock);

                    } else {

                        object.price_chf_store = product_json.data.price_chf_store;
                        object.price_total = Number(object.price_chf_store) * Number($("#quantity").val());
                        object.currency = "CHF";
                        
                        addProductToShopCart(object, total_stock);

                    }

                    //console.log(object);
                   
                } else {

                    $(".onlineshop-add-result").hide().html("<div class='error'>Please choose a quantity!</div>").slideDown();
                    
                }

            } else {

                $(".onlineshop-add-result").hide().html("<div class='error'>"+soldout_message+"</div>").slideDown();
                    
            }

        });

    }

    //Cart
    if(source == "retailer") updateReailerCartCount();
    else updateOnlineShopCartCount();

}

// STORE LOCATOR //
////////////////////////////////////////////////////////////////////////////

// LOAD COUNTRY LIST //

function loadCountryList(country, lang) {

    //Show Map
    $(".content-banner-holder").hide();
    $(".google-map-storelocator").show();

    //Load
    $.post(server + "server-side/services.php", { 
        op: "getDataByRoute",
        route: country

    }, 
                    
    function(data) {

        var json_data = jQuery.parseJSON(data);

        if(json_data.code == 1) {

            //Google Maps
            var gmMapDiv = $("#map-canvas-storelocator");
            var html = "";

            //Markers
            var mapMarkers = [];
            var stores = json_data.data.children;
            stores.sort( function( a, b ) {
                a = a.title.toLowerCase();
                b = b.title.toLowerCase();

                return a < b ? -1 : a > b ? 1 : 0;
            });

            if(json_data.data.route == "switzerland") {
                html += '<div class="storelocator-item"><h1>Capranea Boutique</h1><p>Mühlegasse 18, Gebäude E<br>6340 Baar<br><br><a href="'+lang+'boutique/">view more</a></p></div>';
            }


            for(var i = 0; i < stores.length; i++) {

                var coordinates = stores[i].coordinates.split(',');
                var description = "<p>"+stores[i].short_description+"</p>";
                var $temp = $(description.replace("website", ""));
                $temp.find('a').contents().unwrap().end().end();

                mapMarkers[i] = {latLng:[Number(coordinates[0]), Number(coordinates[1])], data: '<p><strong>'+stores[i].title+'</strong><br>'+$temp.html()+'</p>', options:{ icon: "images/map-marker.png"}};
                html += '<div class="storelocator-item"><h1>'+stores[i].title+'</h1><p>'+stores[i].short_description+'</p></div>';
            }

            if(json_data.data.route == "switzerland") {
                mapMarkers.unshift({latLng:[47.1998435, 8.5248135], data: '<p><strong>Capranea Boutique</strong><br>Mühlegasse 18, Gebäude E<br>6340 Baar</p>', options:{ icon: "images/map-marker.png"}});

            }

            // Map Initial Location
            var initLatitude = mapMarkers[0].latLng[0];
            var initLongitude = mapMarkers[0].latLng[1];
            
            gmMapDiv.gmap3({
                action: "init",
                clear: {
                    name: "marker",
                    all: true
                },
                map: {
                    options: {
                        center:[initLatitude, initLongitude],
                        zoom: 6,
                        zoomControl: true,
                        zoomControlOptions: {
                        style: google.maps.ZoomControlStyle.SMALL
                    },
                    mapTypeControl: false,
                    scaleControl: false,
                    scrollwheel: false,
                    streetViewControl: false,
                    draggable: false,
                    styles: [{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#d3d3d3"}]},{"featureType":"transit","stylers":[{"color":"#808080"},{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"visibility":"on"},{"color":"#b3b3b3"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"weight":1.8}]},{"featureType":"road.local","elementType":"geometry.stroke","stylers":[{"color":"#d7d7d7"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#ebebeb"}]},{"featureType":"administrative","elementType":"geometry","stylers":[{"color":"#a7a7a7"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#efefef"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#696969"}]},{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"visibility":"on"},{"color":"#737373"}]},{"featureType":"poi","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"color":"#d6d6d6"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"color":"#dadada"}]}]
                    }
                },
                marker: { 
                    values:mapMarkers,
                    events:{
                      mouseover: function(marker, event, context){
                        var map = $(this).gmap3("get"),
                          infowindow = $(this).gmap3({get:{name:"infowindow"}});
                        if (infowindow){
                          infowindow.open(map, marker);
                          infowindow.setContent(context.data);
                        } else {
                          $(this).gmap3({
                            infowindow:{
                              anchor:marker, 
                              options:{content: context.data}
                            }
                          });
                        }
                      },
                      mouseout: function(){
                        var infowindow = $(this).gmap3({get:{name:"infowindow"}});
                        if (infowindow){
                          infowindow.close();
                        }
                      }
                    }
                }
            });

            
            //Content
            $(".storelocator-content").html("");
            $(".storelocator-content").html(html);

            var hash_offset = $("#map-canvas-storelocator").offset().top-100;
            $("html, body").animate({
                scrollTop: hash_offset
            });
             

        }

    });

}

// INIT FUNCTIONS //

function storeLocatorInit(lang) {

    //Sort
    $("#country").append($("#country option").remove().sort(function(a, b) {
        if($(a).text() != "LAND") {
            var at = $(a).text(), bt = $(b).text();
            return (at > bt)?1:((at < bt)?-1:0);
        }
    }));

    //Load Country List
    $('#country').change(function() {

        if($('#country').val() != "LAND" && $('#country').val() != "COUNTRY") {
            
            var country = $('#country option:selected').attr("id");
            loadCountryList(country, lang);

        }
        else {

            $(".content-banner-holder").show();
            $(".google-map-storelocator").hide();
            $(".storelocator-content").html("");  

        }

    });

    //Load First
    $('#country').val("Switzerland");
    loadCountryList("Switzerland",lang);
        
}

// STORE LOCATOR //
////////////////////////////////////////////////////////////////////////////

// INIT FUNCTIONS //

function lookbookInit() {

    //If Mobile
    if(mobileTest) {

        $('.lookbook-slider').css("height", "0px");
        $('.lookbook-slider section div').css("height", "0px");
        $('.lookbook-slider section div').removeClass("js-height-full");

        
        if($(".page").height() > $(window).height()) {
            
            $('.lookbook-slider').css("height", $(window).height());
            $('.lookbook-slider section div').css("height", $(window).height());
        
        } else {
            $('.lookbook-slider').css("height", "auto");
            $('.lookbook-slider section div').css("height", "auto");
        }

    }

    //Init Slider
    $(".lookbook-slider").owlCarousel({
        slideSpeed: 350,
        singleItem: true,
        autoHeight: true,
        navigation: true,
        pagination: false,
        navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        afterInit: function() {
            $(".fullwidth-slider").height($(".fullwidth-slider").height() - 50);
        }
    });
}

// ONLINE SHOP //
////////////////////////////////////////////////////////////////////////////

// LOGIN //

function loginOnlineShop(data) {

    post_data = {
        'rdata': data
    };
            
    $.post('includes/onlineshop.php', post_data, function(response){

        if(response != -1) {

            var href = window.location.href;
            href = href.replace("onlineshop-login/", "");
            href = href.replace("https://capranea.com/", "");
            href = href.replace("https://www.capranea.com/", "");
            window.location = server + href + response;
        }

    });

}

// CART //
function updateOnlineShopCart() {

    $.get( "includes/get-onlineshop-cart.php", function( data ) {});

}

function updateOnlineShopCartCount() {

    //Logo
    if(mobileTest) {

        if($(".login-information").length) {
            $(".fm-logo-wrap").hide();
        }
    }

    //Cart
    $.get( "includes/get-onlineshop-cart-json.php", function( data ) {

        if(data != -1) {

            var cart_total = 0;
            var cart = jQuery.parseJSON(data);

            $.each(cart, function(i, obj) {

                cart_total += parseInt(obj.qty);
            });

            $('.login-information span').html(cart_total);
        }

    });

}

function updateOnlineShopCartList(lang) {

    //Reset
    $(".shopping-cart-table tbody").html("");

    //Udpate
    $.get( "includes/get-onlineshop-cart-json.php", function( data ) {
        
        var total  = 0;
        var shipping = 0;
        var currency = "";
        var photo_label = "";
        var prod_label = "";
        var qty_label = "";
        var price_label = "";

        if(lang == "en/") {

            photo_label = "PHOTO";
            prod_label = "PRODUCT";
            qty_label = "Quantity";
            price_label = "UNIT PRICE"; 
            colour_label ="Colour";
            size_label = "Size";

        } else {

            photo_label = "PHOTO";
            prod_label = "PRODUKT";
            qty_label = "Menge";
            price_label = "EINZELPREIS";
            colour_label ="farbe";
            size_label = "Grösse";

        }

        var html = '<tr><th class="hidden-xs">'+photo_label+'</th><th>'+prod_label+'</th><th>'+price_label+'</th><th>TOTAL</th><th></th></tr>';

        //console.log(data);

        $.each(JSON.parse(data), function(i, obj) {


            html += '<tr><td class="hidden-xs"><a href="'+lang+'collection/'+obj.url+'/'+obj.route+'/"><img src="uploaded/'+obj.image+'" width="60" alt=""></a></td><td><b>'+obj.name+'</b><br>'+obj.art_num+'.'+obj.color_code+'.'+obj.size_num+'<br><br><b>'+size_label+':</b> '+obj.size_num+'<br><b>'+colour_label+':</b> '+obj.color+'<br><b>'+qty_label+':</b> '+obj.qty+'<br><br>'+obj.stock_message+'</td>';
            
            html += '<td>'+obj.price_chf_store+' '+obj.currency+'</td>';
            html += '<td>'+obj.price_total+' '+obj.currency+'</td>';
            
            //html += '<td><a href="'+lang+'collection/'+obj.url+'/'+obj.route+'/#'+obj.size_num+'q'+obj.qty+'" title="Edit"><i class="fa fa-pencil-square-o"></i></a></td>';
            html += '<td><a class="b-remove" id="prod'+obj.id+'" title="Remove"><i id="size'+obj.size_num+'" class="fa fa-trash-o"></i></a></td>';


            html += '</tr>';

            total = total + obj.price_total;
            currency = obj.currency;

        });

        $(".shopping-cart-table tbody").html(html);
        $(".b-remove").click(removeProductToShopCart);

        //Udpate Totals
        //Shipping
        if(total < 250 && total != 0) shipping = 6;

        $(".retailer-cart-subtotal span").html(total + " "+currency);
        $(".retailer-cart-shipping span").html(shipping + " "+currency);
        $(".retailer-cart-ordertotal span").html(Number(total + shipping) + " "+currency);


    });

}

function updateConfirmationOnlineShopCartList(lang) {

    //Reset
    $(".shopping-cart-table tbody").html("");

    //Udpate
    $.get( "includes/get-onlineshop-cart-json.php", function( data ) {
        
        var total  = 0;
        var shipping = 0;
        var currency = "";
        var photo_label = "";
        var prod_label = "";
        var qty_label = "";
        var price_label = "";

        if(lang == "en") {

            photo_label = "PHOTO";
            prod_label = "PRODUCT";
            qty_label = "Quantity";
            price_label = "UNIT PRICE"; 
            colour_label ="Colour";
            size_label = "Size";

        } else {

            photo_label = "PHOTO";
            prod_label = "PRODUKT";
            qty_label = "Menge";
            price_label = "EINZELPREIS";
            colour_label ="farbe";
            size_label = "Grösse";

        }

        var html = '<tr><th class="hidden-xs">'+photo_label+'</th><th>'+prod_label+'</th><th>'+price_label+'</th><th>TOTAL</th></tr>';

        //console.log(data);

        $.each(JSON.parse(data), function(i, obj) {

            html += '<tr><td class="hidden-xs"><a href="'+lang+'collection/'+obj.url+'/'+obj.route+'/"><img src="uploaded/'+obj.image+'" width="60" alt=""></a></td><td><b>'+obj.name+'</b><br>'+obj.art_num+'.'+obj.color_code+'.'+obj.size_num+'<br><br><b>'+size_label+':</b> '+obj.size_num+'<br><b>'+colour_label+':</b> '+obj.color+'<br><b>'+qty_label+':</b> '+obj.qty+'<br><br>'+obj.stock_message+'</td>';
            
            html += '<td>'+obj.price_chf_store+' '+obj.currency+'</td>';
            html += '<td>'+obj.price_total+' '+obj.currency+'</td>';

            html += '</tr>';

            total = total + obj.price_total;
            currency = obj.currency;

        });

        $(".shopping-cart-table tbody").html(html);

        //Udpate Totals
        if(total < 250 && total != 0) shipping = 6;//Shipping

        $(".retailer-cart-subtotal span").html(total + " "+currency);
        $(".retailer-cart-shipping span").html(shipping + " "+currency);
        $(".retailer-cart-ordertotal span").html(Number(total + shipping) + " "+currency);
        $(".product_amount").val(Number(total + shipping) + " "+currency);

    });

}

function addProductToShopCart(product, total_stock) {

    $.get( "includes/get-onlineshop-cart-json.php", function( data ) {
         
        var cart = jQuery.parseJSON(data);
        var proceed = true;

        if(findIndexByKeyValues(cart, "id", product.id, "size_num", product.size_num) != null) {

            var index = findIndexByKeyValues(cart, "id", product.id, "size_num", product.size_num);

            if(parseInt(cart[index].qty) >= total_stock) {

                proceed = false;

            } else {

                product.qty = parseInt(cart[index].qty) + 1;
                product.price_total = Number(product.price_chf_store) * Number(product.qty);

                cart.splice(index, 1);

            }
        
        }

        if(proceed) {

            //Add
            cart.push(product);

            //Update Cart
            $.post('includes/set-onlineshop-cart-json.php', 
            {
                'cart_json': JSON.stringify(cart)
            }, 
            function(response){

                //Reset
                //$("#quantity").val("");
                $(".add-cart-info").hide().fadeIn().delay(500).fadeOut();

                //Cart count
                updateOnlineShopCartCount();

            });

        } else {

            $(".stock-cart-info").hide().fadeIn();
            $(".product-qty-stock").hide();

        }

    });

}

function removeProductToShopCart()
{   

    var id = $(this).attr("id") .replace("prod", "");
    var size = $(this).find( "i" ).attr("id") .replace("size", "");
    
    $.get( "includes/get-onlineshop-cart-json.php", function( data ) {
     
        var cart = jQuery.parseJSON(data);
        if(findIndexByKeyValues(cart, "id", id, "size_num", size) != null) {
           
            var index = findIndexByKeyValues(cart, "id", id, "size_num", size);
            cart.splice(index, 1);

            //Update Cart
            $.post('includes/set-onlineshop-cart-json.php', 
            {
                'cart_json': JSON.stringify(cart)
            }, 
            function(response){

                //Cart count
                updateOnlineShopCartCount();

                //Cart List
                updateOnlineShopCartList();

            });

        }
    });
}

// INIT FUNCTION //

//OnlineShop Login
function onlineshopLoginInit(lang) {

    //Update Cart
    updateOnlineShopCartList();

    //Cart count
    updateOnlineShopCartCount();

    //Login
    //Submit Login
    $("#login_btn").click(function() {

        var proceed = true;
        var email = $('input[name=email]').val();
        var password = $('input[name=password]').val();
        var error_pass = "E-Mail-Adresse oder Passwort fehlerhaft";
        var onlineshop_off = "Der Onlineshop steht temporär nicht zur Verfügung. Bitte versuchen Sie es später noch einmal.";
        
        if(lang == "en") {
            error_pass = "Email and/or password incorrect!";
            onlineshop_off = "This Online Shop is temporarily unavailable, please try again later!";

        }
        if (email == "") {
            $('input[name=email]').css('border-color', '#e41919');
            proceed = false;
        }
        if (password == 0) {
            $('input[name=password]').css('border-color', '#e41919');
            proceed = false;
        }

        if (proceed) {

            post_data = {
                'op': 'getBuyer',
                'user': email,
                'pass': password
            };
            
            $.post(server + 'server-side/services.php', post_data, function(response){

                if(response.errorMsg == "OK")
                    loginOnlineShop(response.data);
                else {

                    if(response.code == -1) output = '<div class="error">'+error_pass+'</div>';
                    else if(response.code == -2) output = '<div class="error">'+onlineshop_off+'</div>';

                    $("#result").hide().html(output).slideDown();

                }

            }, 'json');
            
        }
        
        return false;

    })

}

//Recover Password
function onlineshopRecoverInit(lang) {

    //Recover Login
    $("#recover_btn").click(function() {

        var proceed = true;
        var email = $('input[name=email]').val();
        var success_mg = "Ihr Passwort wurde an Ihre angegebene E-Mail-Adresse gesendet.";
        var error_msg = "Die E-Mail-Adresse existiert nicht in unserer Datenbank.";

        if(lang == "en") {
            success_mg = "Your password has been successfully sent to your email.";
            error_msg = "Email address does not exist!";
        }

        if (email == "") {
            $('input[name=email]').css('border-color', '#e41919');
            proceed = false;
        }

        if (proceed) {

            post_data = {
                'op': 'setBuyerPassword',
                'email': email
            };
            
            $.post(server + 'server-side/services.php', post_data, function(response){

                console.log(response);

                if(response.errorMsg == "OK")
                    output = '<div class="success">'+success_mg+'</div>';
                else 
                    output = '<div class="error">'+error_msg+'</div>';
                    

                $("#result").hide().html(output).slideDown();

            }, 'json');
            
        }
        
        return false;

    });

}

//OnlineShop Cart
function onlineShopCartInit(lang) {

    //Update Cart
    updateOnlineShopCartList(lang);

    //Cart count
    updateOnlineShopCartCount();

    //Submit Order
    $("#b-submit").click(function() {

        var cart_empty = "Your shopping cart is empty";
        if(lang == "") cart_empty = "Ihr Warenkorb ist leer";

        if($('.login-information span').html() != "0") {

            post_data = {
                'shipping_json': '{"subtotal":"'+$(".retailer-cart-subtotal span").html()+'", "shipping":"'+$(".retailer-cart-shipping span").html()+'", "ordertotal":"'+$(".retailer-cart-ordertotal span").html()+'"  }'
            };
                
            $.post('includes/set-onlineshop-shipping-json.php', post_data, function(response){

                var href = window.location.href;
                href = href.replace("cart/", "");
                href = href.replace("https://capranea.com/", "");
                href = href.replace("https://www.capranea.com/", "");
                window.location = server + href + response;

            });

        } else {
            $("#result").hide().html('<div class="error">'+cart_empty+'</div>').slideDown();
        }

    });

}

//Shipping
function shippingOnlineShopInit(country) {

    //Country
    $("#country").val(country);
    $("#dcountry").val(country);

    //Delivery Address
    $('#b-delivery').click(function(){
        $(".delivery-section").fadeIn();

        var hash_offset = $("#delivery-section").offset().top-100;
        $("html, body").animate({
            scrollTop: hash_offset
        });
    });

    //Cart count
    updateOnlineShopCartCount();


}

//Confirmation
function onlineshopConfirmationInit(lang) {

    //Update Cart
    updateConfirmationOnlineShopCartList(lang);

    //Cart count
    updateOnlineShopCartCount();

    //Submit
    $("#b-confirmation-submit").click(function() {

        var conditions = $("#conditions").is(':checked');

        if(conditions) {
            $("#payer_message").val($("#message").val());
            $("#payment-form").submit();
        } else {
            $(".conditions-box").addClass("conditions-error");
            //$('html, body').animate({scrollTop: $("#conditions").offset().top }, 500);
        }

    });

}

//Success
function onlineshopSuccessInit() {

    //Update Cart
    updateOnlineShopCartCount();

    //Send Email Order
    post_data = {

        'op': 'setBuyerOrder',
        'invoice': onlineshop_invoice
            
    };

    $.post(server + 'server-side/services.php', post_data, function(response){  console.log(response); }, 'json');

}

//Edit Account
function onlineShopEditAccountInit(title, country) {

    //Cart count
    updateOnlineShopCartCount();

    //Selects
    $("#title").val(title);
    $("#country").val(country);
    
}

//Returns
function onlineshopReturnsInit(lang) {

    //Cart count
    updateOnlineShopCartCount();

    //Langs
    var no_order = "No order available with this reference number!";
    var insert_ref = "Please insert your order reference number!";

    if(lang == "de") {
        no_order = "Es existiert keine Bestellung mit dieser REFERENCE NUMBER.";
        insert_ref = "Bitte geben Sie die REFERENCE NUMBER Ihrer Bestellung ein.";
    }

    //Submit
    $("#send_btn").click(function() {

        if($("#refnumber").val() != "") {

            post_data = {

                'op': 'getBuyerOrder',
                'order_number': $("#refnumber").val()
            };

            //Ajax post data to server
             $.post(server + 'server-side/services.php', post_data, function(response){

                if(response.errorMsg == "OK") {

                    $("#order-information").show();

                    var data = $.parseJSON(response.data);

                    //Reset
                    $(".shopping-cart-table tbody").html("");

                    var total  = 0;
                    var shipping = 0;
                    var currency = "";
                    var order_products = [];
                    var n = 0;

                    var photo_label = "";
                    var prod_label = "";
                    var qty_label = "";
                    var price_label = "";

                    if(lang == "de") {

                        colour_label ="farbe";
                        size_label = "Grösse";
                        photo_label = "PHOTO";
                        prod_label = "PRODUKT";
                        qty_label = "Menge";
                        price_label = "EINZELPREIS";
                        select_title = 'WAST IST DER GRUND FÜR IHRE RÜCKSENDUNG?';
                        select_box = '<option value="Product do not meet expectations">Das Produkt erfüllt meine Erwartungen nicht.</option><option value="Incorrect size ordered">Falsche Grösse bestellt</option><option value="Incorrect size delivered">Falsche Grösse geliefert</option> <option value="Item is damaged">Artikel ist beschädigt (bitte beschreiben Sie den Defekt im untenstehenden Feld Bemerkungen)</option><option value="Other">andere Gründe</option>';

                    } else {

                        colour_label ="Colour";
                        size_label = "Size";
                        photo_label = "PHOTO";
                        prod_label = "PRODUCT";
                        qty_label = "Quantity";
                        price_label = "UNIT PRICE"; 
                        select_title = 'WHAT IS THE REASON FOR THE RETURN?';
                        select_box = '<option value="Product do not meet expectations">Product do not meet expectations</option><option value="Incorrect size ordered">Incorrect size ordered</option><option value="Incorrect size delivered">Incorrect size delivered</option><option value="Item is damaged">Item is damaged (please describe the damage in the provided field notes below)</option><option value="Other">Other</option>';

                    }

                     var html = '<tr><th class="hidden-xs">'+photo_label+'</th><th>'+prod_label+'</th><th>'+price_label+'</th><th></th></tr>';

                    $.each(data[1], function(i, obj) {

                        for (z = 0; z < obj.qty; z++) { 

                            html += '<tr><td class="hidden-xs"><img src="uploaded/'+obj.image+'" width="60" alt=""></td><td><b>'+obj.name+'</b><br>'+obj.art_num+'.'+obj.color_code+'.'+obj.size_num+'<br><br><b>'+size_label+':</b> '+obj.size_num+'<br><b>'+colour_label+':</b> '+obj.color+'<br><b>'+qty_label+':</b>1<br><br>';

                            html += '<h6>'+select_title+'</h6><select class="input-md form-control" id="reason'+n+'" name="reason">'+select_box+'</select></td>';

                            html += '<td>'+obj.price_chf_store+' '+obj.currency+'</td>';
                            //html += '<td>'+obj.price_total+' '+obj.currency+'</td>';
                            html += '<td><input type="checkbox" class="products_check" name="products" id="product'+n+'" value="1"></td>';
                            html += '</tr>';
                            
                            order_products.push(data[1][i]);
                            n++;

                        }

                    });

                    $(".shopping-cart-table tbody").html(html);

                    //Check Boxes
                    /*$("#product0").prop('checked', true); 
                    $(".products_check").click(function(){
                        $('.products_check').each(function(){
                            $(this).prop('checked', false); 
                        }); 
                        $(this).prop('checked', true);
                    });

                    $("#reason").change(function(){
                        $('#other').prop('disabled', true);
                        $('#other').val("");
                        if ($(this).val() == 'Other') {
                            $('#other').prop('disabled', false);
                        }
                    });*/
                    
                    $(".submit_btn").click(function(){

                        var return_products = [];

                        var $boxes = $('input[name=products]:checked');
                        $boxes.each(function(){

                            var id = $(this).attr("id").replace("product","");
                            order_products[id]["reason_msg"] = $("#reason"+id).val();
                            return_products.push(order_products[id]);

                        });

                        var message = '<b>Bemerkungen</b><br><br>' + $('#notes').val();
                        //if(message == "Other") message = $("#other").val();

                        if(return_products.length > 0) {

                            post_data = {

                                'op': 'setBuyerReturns',
                                'return_products': JSON.stringify(return_products),
                                'buyer_information' : JSON.stringify(data[3][0]),
                                'user_information' : JSON.stringify(data[4][0]),
                                'order_reference' : data[0][0].order_reference,
                                'buyer_msg': message

                            };

                            //Ajax post data to server
                            $.post(server + 'server-side/services.php', post_data, function(response){

                                if(response.errorMsg == "OK") {

                                    $("#reference-form").hide();
                                    $("#order-information").hide();
                                    $("#returns-success").show();
                                    $(".rnumber").html(response.data);
                                    $(".rcustomer").html(data[3][0].billingTitle + ' ' + data[3][0].billingName + ' ' + data[3][0].billingSurname);
                                    $(".rcustomerid").html(padLeft(data[4][0].id,5));

                                    var hash_offset = $("#returns-success").offset().top-100;
                                    $("html, body").animate({
                                        scrollTop: hash_offset
                                    });

                                }

                            }, 'json');

                        }                        

                    });
                    
                    var hash_offset = $("#order-information").offset().top-100;
                    $("html, body").animate({
                        scrollTop: hash_offset
                    });


                } else {

                    $("#result").hide().html('<div class="error">'+no_order+'</div>').slideDown();
                }

            }, 'json');

        } else {

            $("#result").hide().html('<div class="error">'+insert_ref+'</div>').slideDown();

        }

    });

}

// RETAILER //
////////////////////////////////////////////////////////////////////////////

// LOGIN //

function loginRetailer(data) {

    post_data = {
        'rdata': data
    };
            
    $.post('includes/retailer.php', post_data, function(response){

        if(response == 1) {

            window.location = server + "en/";
        }

    });

}

// CART //

function getRetailerCart() {

    $.get( "includes/get-retailer-cart.php", function( data ) {
     

    });


}

function updateReailerCartCount() {

    //Logo
    if(mobileTest) {

        if($(".login-information").length) {
            $(".fm-logo-wrap").hide();
        }
    }

    //Cart
    $.get( "includes/get-retailer-cart-json.php", function( data ) {

        if(data != -1) {
            var cart_total = 0;
            var cart = jQuery.parseJSON(data);

            $.each(cart, function(i, obj) {

                cart_total += parseInt(obj.qty);
            });

            $('.login-information span').html(cart_total);
        }

    });

}

function updateRetailerCartList() {

    //Reset
    $(".shopping-cart-table tbody").html("");

    //Udpate
    $.get( "includes/get-retailer-cart-json.php", function( data ) {
        
        var html = '<tr><th class="hidden-xs">PHOTO</th><th>PRODUCT</th><th>UNIT PRICE</th><th>TOTAL</th><th></th></tr>';
        var total  = 0;
        var shipping = 0;
        var currency = "";

        console.log(data);

        $.each(JSON.parse(data), function(i, obj) {


            html += '<tr><td class="hidden-xs"><a href="en/order-zone/'+obj.url+'/'+obj.route+'/"><img src="uploaded/'+obj.image+'" width="60" alt=""></a></td><td><b>'+obj.name+'</b><br>'+obj.art_num+'.'+obj.color_code+'.'+obj.size_num+'<br><br><b>Size:</b> '+obj.size_num+'<br><b>Colour:</b> '+obj.color+'<br><b>Quantity:</b> '+obj.qty+'<br><br>'+obj.stock_message+'</td>';
            
            if(obj.currency == "EUR") {
                html += '<td>€ '+obj.price_eur_retailer+'</td>';
                html += '<td>€ '+obj.price_total+'</td>';
            }
            else {
                html += '<td>'+obj.price_chf_retailer+' '+obj.currency+'</td>';
                html += '<td>'+obj.price_total+' '+obj.currency+'</td>';
            }
            
            html += '<td><a class="b-remove" id="prod'+obj.id+'" title="Remove"><i id="size'+obj.size_num+'" class="fa fa-trash-o"></i></a></td>';


            html += '</tr>';

            total = total + obj.price_total;
            currency = obj.currency;

        });

        $(".shopping-cart-table tbody").html(html);
        $(".b-remove").click(removeProductToCart);

        //Udpate Totals
        if(currency == "EUR") {

            //Shipping
            if(total < 300 && total != 0) shipping = 9.5;

            $(".retailer-cart-subtotal span").html("€ " + total + "");
            $(".retailer-cart-shipping span").html("€ " + shipping+ "");
            $(".retailer-cart-ordertotal span").html("€ " + Number(total + shipping)+ "");
        }
        else {

            //Shipping
            if(total < 250 && total != 0) shipping = 11;

            $(".retailer-cart-subtotal span").html(total + " "+currency);
            $(".retailer-cart-shipping span").html(shipping + " "+currency);
            $(".retailer-cart-ordertotal span").html(Number(total + shipping) + " "+currency);
        }

    });

}

function updateConfirmationCartList() {

    //Reset
    $(".shopping-cart-table tbody").html("");

    //Udpate
    $.get( "includes/get-retailer-cart-json.php", function( data ) {
        
        var html = '<tr><th class="hidden-xs">PHOTO</th><th>PRODUCT</th><th>UNIT PRICE</th><th>TOTAL</th></tr>';
        var total  = 0;
        var shipping = 0;
        var currency = "";

        //console.log(data);

        $.each(JSON.parse(data), function(i, obj) {

            html += '<tr><td class="hidden-xs"><a href="en/order-zone/'+obj.url+'/'+obj.route+'/"><img src="uploaded/'+obj.image+'" width="60" alt=""></a></td><td><b>'+obj.name+'</b><br>'+obj.art_num+'.'+obj.color_code+'.'+obj.size_num+'<br><br><b>Size:</b> '+obj.size_num+'<br><b>Colour:</b> '+obj.color+'<br><b>Quantity:</b> '+obj.qty+'<br><br>'+obj.stock_message+'</td>';
            
            if(obj.currency == "EUR") {
                html += '<td>€ '+obj.price_eur_retailer+'</td>';
                html += '<td>€ '+obj.price_total+'</td>';
            }
            else {
                html += '<td>'+obj.price_chf_retailer+' '+obj.currency+'</td>';
                html += '<td>'+obj.price_total+' '+obj.currency+'</td>';
            }


            html += '</tr>';

            total = total + obj.price_total;
            currency = obj.currency;

        });

        $(".shopping-cart-table tbody").html(html);

        //Udpate Totals
        if(currency == "EUR") {

            //Shipping
            if(total < 300 && total != 0) shipping = 9.5;

            $(".retailer-cart-subtotal span").html("€ " + total + "");
            $(".retailer-cart-shipping span").html("€ " + shipping+ "");
            $(".retailer-cart-ordertotal span").html("€ " + Number(total + shipping)+ "");
        }
        else {

            //Shipping
            if(total < 250 && total != 0) shipping = 11;

            $(".retailer-cart-subtotal span").html(total + " "+currency);
            $(".retailer-cart-shipping span").html(shipping + " "+currency);
            $(".retailer-cart-ordertotal span").html(Number(total + shipping) + " "+currency);
        }

    });

}

function addProductToCart(product, total_stock) {


    $.get( "includes/get-retailer-cart-json.php", function( data ) {
         
        var cart = jQuery.parseJSON(data);
        var proceed = true;

        if(findIndexByKeyValues(cart, "id", product.id, "size_num", product.size_num) != null) {
            var index = findIndexByKeyValues(cart, "id", product.id, "size_num", product.size_num);

            if(parseInt(cart[index].qty) >= total_stock) {

                proceed = false;

            } else {

                product.qty = parseInt(cart[index].qty) + 1;

                if(product.currency == "EUR")
                    product.price_total = Number(product.price_eur_retailer) * product.qty;
                else 
                    product.price_total = Number(product.price_chf_retailer) * product.qty;

                cart.splice(index, 1);

            }
        }

        if(proceed) {

            //Add
            cart.push(product);

            //Update Cart
            $.post('includes/set-retailer-cart-json.php', 
            {
                'cart_json': JSON.stringify(cart)
            }, 
            function(response){

                //Reset
                //$("#quantity").val("");
                $(".add-cart-info").hide().fadeIn().delay(500).fadeOut();

                //Cart count
                updateReailerCartCount();

            });

        } else {

            $(".stock-cart-info").hide().fadeIn();
            $(".product-qty-stock").hide();

        }

    });

} 

function removeProductToCart()
{   

    var id = $(this).attr("id") .replace("prod", "");
    var size = $(this).find( "i" ).attr("id") .replace("size", "");
    
    $.get( "includes/get-retailer-cart-json.php", function( data ) {
     
        var cart = jQuery.parseJSON(data);
        if(findIndexByKeyValues(cart, "id", id, "size_num", size) != null) {
           
            var index = findIndexByKeyValues(cart, "id", id, "size_num", size);
            cart.splice(index, 1);

            //Update Cart
            $.post('includes/set-retailer-cart-json.php', 
            {
                'cart_json': JSON.stringify(cart)
            }, 
            function(response){

                //Cart count
                updateReailerCartCount();

                //Cart List
                updateRetailerCartList();

            });

        }
    });
}

function findIndexByKeyValue(obj, key, value)
{
    for (var i = 0; i < obj.length; i++) {
        if (obj[i][key] == value) {
            return i;
        }
    }
    return null;
}

function findIndexByKeyValues(obj, key1, value1, key2, value2)
{
    for (var i = 0; i < obj.length; i++) {
        if (obj[i][key1] == value1 && obj[i][key2] == value2) {
            return i;
        }
    }
    return null;
}

// INIT FUNCTIONS //

//Login
function retailerLoginInit() {

    //Submit Login
    $("#login_btn").click(function() {

        var user = $('input[name=username]').val();
        var password = $('input[name=password]').val();

        var proceed = true;

        if (user == "") {
            $('input[name=username]').css('border-color', '#e41919');
            proceed = false;
        }
        if (password == 0) {
            $('input[name=password]').css('border-color', '#e41919');
            proceed = false;
        }

        if (proceed) {

            post_data = {
                'op': 'getRetailer',
                'user': user,
                'pass': password
            };
            
            $.post(server + 'server-side/services.php', post_data, function(response){

                if(response.errorMsg == "OK")
                    loginRetailer(response.data);
                else {

                    output = '<div class="error">Username and/or password incorrect!</div>';
                    $("#result").hide().html(output).slideDown();

                }

            }, 'json');
            
        }
        
        return false;

    });
}

//Recover
function retailerRecoverInit() {

    //Recover Login
    $("#recover_btn").click(function() {

        var email = $('input[name=email]').val();

        var proceed = true;

        if (email == "") {
            $('input[name=email]').css('border-color', '#e41919');
            proceed = false;
        }

        if (proceed) {

            post_data = {
                'op': 'setPassword',
                'email': email
            };
            
            $.post(server + 'server-side/services.php', post_data, function(response){

                if(response.errorMsg == "OK")
                    output = '<div class="success">Your password has been successfully sent to your email.</div>';
                else 
                    output = '<div class="error">Email address does not exist!</div>';
                    

                $("#result").hide().html(output).slideDown();

            }, 'json');
            
        }
        
        return false;

    });

}

//Retailer Cart
function retailerCartInit() {

    //Update Cart
    updateRetailerCartList();

    //Cart count
    updateReailerCartCount();

    //Submit Order
    $("#b-submit").click(function() {

        if($('.login-information span').html() != "0") {

            post_data = {
                'shipping_json': '{"subtotal":"'+$(".retailer-cart-subtotal span").html()+'", "shipping":"'+$(".retailer-cart-shipping span").html()+'", "ordertotal":"'+$(".retailer-cart-ordertotal span").html()+'"  }'
            };
                
            $.post('includes/set-retailer-shipping-json.php', post_data, function(response){

                window.location = server + "en/retail-area-shipping/";

            });

        } else {
            $("#result").hide().html('<div class="error">Your shopping cart is empty</div>').slideDown();
        }

    });

}

//Shipping
function shippingInit() {

    //Delivery Address
    $('#b-delivery').click(function(){
        $(".delivery-section").fadeIn();

        var hash_offset = $("#delivery-section").offset().top-100;
        $("html, body").animate({
            scrollTop: hash_offset
        });
    });

    //Cart count
    updateReailerCartCount();


}

//Confirmation
function retailerConfirmationInit() {


    //Update Cart
    updateConfirmationCartList();

    //Cart count
    updateReailerCartCount();

    //Submit
    $("#b-confirmation-submit").click(function() {

        post_data = {

            'op': 'setOrder',
            'products_cart': products_cart,
            'shipping_info': shipping_info,
            'address_info': order_info
        };

        console.log(post_data); 

        //Ajax post data to server
         $.post(server + 'server-side/services.php', post_data, function(response){

            if(response.errorMsg == "OK") {

                window.location = server + "en/retail-area-success/";

            }

        }, 'json');

    });
    

}

//Downloads
function downloadsRetailerInit() {
    
    //Images
    $(".image-select").click(function(){
        
        var image_id = $(this).attr("title");
        var image_title = $("#image-select"+image_id).find("span").text();
        var images_holder_content = $("#images-holder").val();

        if(images_holder_content.indexOf(image_title) == -1) {
            $("#images-holder").val(images_holder_content + image_title + ";");
            $("#image-select"+image_id).css("color", "#959595");
        } else {

            images_holder_content = $("#images-holder").val();
            images_holder_content = images_holder_content.replace(image_title+';','');
            $("#images-holder").val(images_holder_content);
            $("#image-select"+image_id).css("color", "#000000");
        }

    });

}

//Reset Cart
function resetRetailerCart() {

    
}