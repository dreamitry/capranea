/* ---------------------------------------------
 Contact form
 --------------------------------------------- */
$(document).ready(function(){
    $("#submit_btn").click(function(){
        
        //get input field values
        var user_title = $('select[name=title]').val();
        var user_fname = $('input[name=fname]').val();
        var user_name = $('input[name=name]').val();
        var user_email = $('input[name=email]').val();
        var user_phone = $('input[name=phone]').val();
        var user_address = $('input[name=address]').val();
        var user_city = $('input[name=city]').val();
        var user_country = $('input[name=country]').val();
        var user_message = $('textarea[name=message]').val();
        
        //simple validation at client's end
        //we simply change border color to red if empty field using .css()
        var proceed = true;

        if (user_title == 0) {
            $('select[name=title]').css('border-color', '#e41919');
            proceed = false;
        }
        if (user_fname == "") {
            $('input[name=fname]').css('border-color', '#e41919');
            proceed = false;
        }
        if (user_name == "") {
            $('input[name=name]').css('border-color', '#e41919');
            proceed = false;
        }
        if (user_email == "") {
            $('input[name=email]').css('border-color', '#e41919');
            proceed = false;
        }
        if (user_phone == "") {
            $('input[name=phone]').css('border-color', '#e41919');
            proceed = false;
        }
        if (user_address == "") {
            $('input[name=address]').css('border-color', '#e41919');
            proceed = false;
        }
        if (user_city == "") {
            $('input[name=city]').css('border-color', '#e41919');
            proceed = false;
        }
        if (user_country == "") {
            $('input[name=country]').css('border-color', '#e41919');
            proceed = false;
        }
        if (user_message == "") {
            $('textarea[name=message]').css('border-color', '#e41919');
            proceed = false;
        }
        
        //everything looks good! proceed...
        if (proceed) {
            //data to be sent to server
            post_data = {
                'userTitle': user_title,
                'userFName': user_fname,
                'userName': user_name,
                'userEmail': user_email,
                'userPhone': user_phone,
                'userAddress': user_address,
                'userCity': user_city,
                'userCountry': user_country,
                'userMessage': user_message
            };
            
            //Ajax post data to server
            $.post('includes/send-contact.php', post_data, function(response){
            
                //load json data from server and output message     
                 if (response == -1) {
                    output = '<div class="error"></div>';
                }
                else {
                
                    output = '<div class="success">Danke für Ihre Anfrage. Wir melden uns schnellstmöglich bei Ihnen.<br>Liebe Grüsse, Capranea</div>';
                    
                    //reset values in all input fields
                    $('#contact_form select').val('');
                    $('#contact_form input').val('');
                    $('#contact_form textarea').val('');
                }
                
                $("#result").hide().html(output).slideDown();
            }, 'json');
            
        }
        
        return false;
    });
    
    //reset previously set border colors and hide all message on .keyup()
    $("#contact_form input, #contact_form textarea").keyup(function(){
        $("#contact_form input, #contact_form textarea").css('border-color', '');
        $("#result").slideUp();
    });

    $("#contact_form select").change(function(){
        $("#contact_form select").css('border-color', '');
        $("#result").slideUp();
    });
    
});
