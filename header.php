	<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Capranea
 */

?>
<?php 
$blog_id = get_current_blog_id();

$lobbylinkretail = 'https://capranea.com/finalstaging/retail/my-account';
$lobbylinkPress = 'https://capranea.com/finalstaging/press/my-account';
$homePress = 'https://capranea.com/finalstaging/press/';

if (is_user_logged_in()) {

	if (4 === $blog_id && (is_page(7))) {
		wp_redirect( $homePress); 
		  exit;
	}
	
} else {
	if (2 === $blog_id && !(is_page(412))) {
		wp_redirect( $lobbylinkretail); 
		  exit;
	}

	if  (4 === $blog_id && !(is_page(7))) {
		wp_redirect( $lobbylinkPress); 
		  exit;
	}

	
};	
$wmc_currentcurrency = sanitize_text_field($_GET['wmc-currency']);
$current_user_id = get_current_user_id();
$wmc_therightcurrency =  get_user_meta($current_user_id, 'price_list_currency', true);  
global $wp;
$current_url  = home_url( $wp->request );
/* lobby awaiting for the account to be approved */
if  (2 === $blog_id && $current_url != $lobbylinkretail ) {
	if ( empty($wmc_therightcurrency)) {
		wp_redirect( $lobbylinkretail);
		exit;
	}
	/* check if you're not in the lobby and the right currency isn't set i.e. your account been approved*/
}
if (2 === $blog_id ) {
	if  ($wmc_currentcurrency != $wmc_therightcurrency) {
		$right_currency_location = $current_url . '?wmc-currency=' . $wmc_therightcurrency;
			wp_safe_redirect( $right_currency_location );
			exit;
	} ;
};

?>



<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/assets/img/meta/favicon.png">
	<link rel="apple-touch-icon" href="<?php bloginfo('stylesheet_directory'); ?>/assets/img/meta/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo('stylesheet_directory'); ?>/assets/img/meta/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo('stylesheet_directory'); ?>/assets/img/meta/apple-touch-icon-114x114.png">
	<?php wp_head(); ?>


	

	<!-- Favicons -->


	<link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400" rel="stylesheet">

	<script src="https://cdn.jsdelivr.net/npm/vue@2.5.17/dist/vue.js"></script>
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>




</head>

<body>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/de_DE/sdk.js#xfbml=1&version=v3.1&appId=249758585738455&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
	<div class="menu-container">
		<div class="logo-container">
			<a href="<?php echo get_site_url(); ?>"><img id="logo-name" class="logo" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/logo-name.png"></a>
			<img id="logo" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/logo.png">
		</div>

		<style>
		.menu-item-has-children {
			position: relative;
			display: inline-block;
		}


		

	


		.language-switcher .sub-menu {
			visibility: hidden;
			display: flex !important;
			position: absolute;
			padding: 2px;
			z-index: 1;
		}

		.language-switcher:hover .sub-menu {
			visibility:	visible;
		
		
		}
	
	


		.sub-menu {
			display: none;
		
			background-color: #838383;
		

		}

		.menu-item-has-children:hover .sub-menu {
			display:	flex;
			flex-direction: column;
		
		}

		.sub-menu:hover {
			display:	flex;
			flex-direction: column;
		
		}
		
		@media (max-width: 700px ) {
			.sub-menu a {
				color: #FAFAFA;
			}
		}

		#dropdown-languages {
	display: none;
    position: absolute;
    width: 25px;
    padding: 0 0 5px 0;
    top: 24px;
    left: -2px;
	background-color: rgba(100, 100, 100, 0.8);
	padding: 2px;
	
}


.language-switcher a {
	font-size: 8pt !important;
	
	border-radius: 50%;
	padding: 3px;
	background-color: #808080 !important;
}

.dropdown-trigger-languages {
	width: 25px;
	height: 25px;
}



.language-switcher:hover .sub-menu {
			visibility:	visible;
		
}
.dropdown-trigger:hover #dropdown-languages {
	display: block;
}



/* specific menu css */

.menu-item-has-children .sub-menu {  
    position: absolute;
	left: -4px;
	top: 24px;
	padding: 0 5px 5px 5px;
}

		</style>
		<div class="menu-content">
			<div class="menu-inner-container">
				<div class="menu-links">

				<?php 

					if ($blog_id === 2 or $blog_id === 4 ) {
						if (is_user_logged_in()) {
						
						} else {
							global $switched;
							switch_to_blog(1);
						}
					}

					/* switching the loop to the main site for the lookbook to come from one source */  
					?>


						<?php 
							wp_nav_menu(
								array(
									'menu' => 'Primary',
									'container' => ' ',
								)
								);
						?>

						<?php restore_current_blog(); //switched back to main site (check above ?>


			<?php 
 
			/* commeneted out if they change their mind again switch ($blog_id) {
				case "3":
				echo '	<div class="dropdown-trigger-languages">
				<div class="language-switcher-circle">
					<a href="#">EN</a>
				</div>

				<div class="language-switcher-options-container">
					<div class="language-switcher-option">
						<a href="https://capranea.com/finalstaging/">DE</a>
					</div>
				</div>
			</div>';
			break;
				case "1":
					echo '	<div class="dropdown-trigger-languages">
					<div class="language-switcher-circle">
						<a href="#">DE</a>
					</div>
	
					<div class="language-switcher-options-container">
						<div class="language-switcher-option">
							<a href="' . get_home_url() . '/en/">EN</a>
						</div>
					</div>
				</div>';
					break;
				case "2":
					echo " ";
					break;
				
			} */
			?>

		
				</div>				
			</div>
		</div>
	</div>
	<div class="menu-container-dummy">

	</div>
	<div class="responsive-menu-container">
		<div class="responsive-menu-content">
		<style> 

		@media (max-width: 700px) {
						#menuToggle
						{
						display: block;
						position: relative;
						
						
						z-index: 1;
						
						-webkit-user-select: none;
						user-select: none;
						}
						#menuToggle input
						{
						display: block;
						width: 40px;
						height: 32px;
						position: absolute;
						top: -7px;
						left: -5px;
						
						cursor: pointer;
						
						opacity: 0; 
						z-index: 2; 
						
						-webkit-touch-callout: none;
						}
						#menuToggle span
						{
						display: block;
						width: 33px;
						height: 4px;
						margin-bottom: 5px;
						position: relative;
						
						background: black;
						
						z-index: 1;
						
						transform-origin: 4px 0px;
						
						transition: transform 0.5s cubic-bezier(0.77,0.2,0.05,1.0),
									background 0.5s cubic-bezier(0.77,0.2,0.05,1.0),
									opacity 0.55s ease;
						}
						#menuToggle span:first-child
						{
						transform-origin: 0% 0%;
						}
						#menuToggle span:nth-last-child(2)
						{
						transform-origin: 0% 100%;
						}
						#menuToggle input:checked ~ span
						{
							opacity: 1;
							transform: rotate(45deg) translate(-2px, -1px);
							background: #999;
						}
						#menuToggle input:checked ~ span:nth-last-child(3)
						{
							opacity: 0;
							transform: rotate(0deg) scale(0.2, 0.2);
						}

						/*
						* Ohyeah and the last one should go the other direction
						*/
						#menuToggle input:checked ~ span:nth-last-child(2)
						{
						transform: rotate(-45deg) translate(0, -1px);
						}

						/*
						* Make this absolute positioned
						* at the top left of the screen
						*/
						#menu-primary-1
						{
						position: absolute;
						width: calc(100vw + 100px);
						height: calc(100vh + 100px);
						margin: -100px 0 0 -50px;
						padding: 50px;
						padding-top: 125px;
						background: #141414;
						list-style-type: none;
						-webkit-font-smoothing: antialiased;
						/* to stop flickering of text in safari */
						transform-origin: 0% 0%;
						transform: translate(-100%, 0);
						transition: transform 0.5s cubic-bezier(0.77,0.2,0.05,1.0);
						}
						#menu-primary-1 li
						{
						padding: 0px !important;
						font-size: 22px;
						display: block;
						position: relative;
						margin: 0;
						padding: 10px 40px;
						color: #999;
						}
						#menu-primary-1 .sub-menu {
							position: static;
							display: initial;
							padding-left: 10px;
						}
						#menu-primary-1 .sub-menu li{
							background-color: #141414;
						}

						#menu-primary-1 .sub-menu li a{
							font-size: 12px !important;
						}

						.sub-menu {
							background-color: #141414 !important;
						}
						#menu-primary-1 a, #menu-primary-1 a:active, #menu-primary-1 a:visited {
							color: black;
							text-decoration: none;	
							text-transform: uppercase;	
							color: #999;
							font-size: 22px;
							letter-spacing: 2px;
							font-weight: 400;
							text-transform: uppercase;						
						}
						#menuToggle input:checked ~ ul
						{
						transform: none;
						}
						
						#menu-primary-1 .menu-language-item a {

						color: #da9443 !important;

						}

						#menu-primary-1 .menu-item-878, #menu-primary-1 .menu-item-593, #menu-primary-1 .menu-item-1966, #menu-primary-1 .menu-item-1969 {
							margin-bottom: 25px;
						}
		}
	</style>
				<nav role="navigation" style=" display: flex; align-items: center;">
					<div id="menuToggle">
						<!--
						A fake / hidden checkbox is used as click reciever,
						so you can use the :checked selector on it.
						-->
						<input type="checkbox" />
						
						<!--
						Some spans to act as a hamburger.
						
						They are acting like a real hamburger,
						not that McDonalds stuff.
						-->
						<span></span>
						<span></span>
						<span></span>
						
						<!--
						Too bad the menu has to be inside of the button
						but hey, it's pure CSS magic.
						-->
						
						<?php 

if ($blog_id === 2 or $blog_id === 4 ) {
	if (is_user_logged_in()) {
	
	} else {
		global $switched;
		switch_to_blog(1);
	}
}

/* switching the loop to the main site for the lookbook to come from one source */  
?>
						<?php 
							wp_nav_menu(
								array(
									'menu' => 'Primary',
									'container' => 'ul',
									'depth' => 2,
								)
								);
						?>
									
					</div>
					</nav>

			<div class="responsive-logo-menu">
			<a href="<?php echo get_site_url(); ?>"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/logo-name.png"></a>
					</div>
					<?php restore_current_blog(); //switched back to main site (check above ?>

		</div>
	</div>





