<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Capranea
 */

get_header();
?>

 <div class="content-page-header">
			<h2><?php single_cat_title(); ?><h2>
</div>
 
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="archive-container">

		


	

			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
				echo '<article id="post">
				<div class="post-container">
						
						<div class="entry-content">';
				
					
						
					echo '</header>';
					echo '<div class="post-category-page-flex">';
					 capranea_post_thumbnail();

					 echo '<div><header class="entry-header">';
					
					 if ( is_singular() ) :
						 the_title( '<h1 class="entry-title">', '</h1><br>' );
					 else :
						 the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2><br>' );
					 endif;
			 
					 if ( 'post' === get_post_type() ) :
						 
						 
					  endif;
					the_excerpt(); 
					echo '<div>';
						
				
						wp_link_pages( array(
							'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'capranea' ),
							'after'  => '</div>',
						) );
						
					echo '</div></div><!-- .entry-content --></article><!-- #post-<?php the_ID(); ?> -->';

			endwhile;


		?>

		</main><!-- #main -->
	</div><!-- #primary -->
	</div>

<?php

get_footer();
