<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Capranea
 */

?>

	</div><!-- #content -->
    

<footer class="with-social-bar">
    <div class="social-bar-container">
        <div class="social-col">
            <h4>STOREFINDER</h4>
          <a href="https://capranea.com/finalstaging/storefinder/"> <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/world-map.png"> </a>


        </div>
        <div class="social-col">
            <h4>INSTAGRAM</h4>
            <a href="https://www.instagram.com/capranea/"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/instagram-img.png"></a>
        </div>
        <div class="social-col">
            <h4><?php 
               $blog_id3 = get_current_blog_id();
               if ($blog_id3 === 2 or $blog_id3 === 3 ) {
                            echo ' CONTACT';  } else {
                                
                                echo 'KONTAKT' ;
                            }?> </h4>
            <p>CAPRANEA SPORTS AG<br>
            Mühlegasse 18, Gebäude E, 6340 Baar,<br>
             Switzerland<br>
             <br>
            info@capranea.com</p>
            <br>
        </div>
    </div>
</footer>

<footer class="no-social-bar">
    <div class="logo-footer">
        <a href="<?php get_home_url(); ?>"><img id="logo-name" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/logo-footer.png"></a>
    </div>
    <div class="footer-container">
        <div class="footer-content">
            
                <?php 
                    wp_nav_menu(
                        array(
                            'menu' => 'footer',
                            'container' => ' ',
                        )
                        );
                ?>


            
        </div>
    </div>
    <div class="copy">
        <p>&copy; CAPRANEA SPORTS AG, 2018.</p>
    </div>

</footer>


</body>

  


</div><!-- #page -->

<?php wp_footer(); ?>
<?php 

$blog_id = get_current_blog_id();

if (3 == $blog_id && !is_user_logged_in()  ) {
	include(dirname(__FILE__) . "/inc/retailer-login-form-footer.php");

} 

if ( is_front_page()) {
	include(dirname(__FILE__) . "/inc/home-vue-inst.php");
 }

?>	



<script
src="https://code.jquery.com/jquery-3.3.1.min.js"
integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
crossorigin="anonymous"></script>

<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>



<script>

$( ".variations_form" ).on( "woocommerce_variation_select_change", function () {
	alert( "Options changed" );
} );

function openNav() {
  document.getElementById("responsive-menu-links").style.display = "inline-block";
}

function closeNav() {
  document.getElementById("responsive-menu-links").style.display = "none";

}


$(document).ready(function() {
    $(".product-materials-content").click(function() {      
        if($(this).hasClass("materials-content-open")) {
            $(this).removeClass("materials-content-open");
        } else {
            $(this).addClass("materials-content-open");
        }

    })
});


$(document).ready(function() {
    $(".login-form-content").click(function() {      
      
            $(this).addClass("login-form-open");
        

    })
});


var initialSrc = "<?php bloginfo('stylesheet_directory'); ?>/assets/images/logo-name.png";

$(window).scroll(function() {
   var value = $(this).scrollTop();
   if (value > 100)
      $("#logo-name").attr("src", "<?php bloginfo('stylesheet_directory'); ?>/assets/images/logo-small-dark.png");

   else
      $("#logo-name").attr("src", initialSrc);
      

});





	





</script>


<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>


<script type="text/javascript">
    $(document).ready(function(){
      $('.slider-container').slick({
        accessibility: true,
		arrows: true,
        nextArrow: '<div class="top-slider-arrow top-slider-arrow-right"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/001-right-arrow.svg"></div>',
  		prevArrow: '<div class="top-slider-arrow top-slider-arrow-left"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/001-left-arrow.svg"></div>',
        autoplay: true,
        slidesToScroll: 1,
  		autoplaySpeed: 3000,
        infinite: true,
        speed: 700,
        fade: true,
        cssEase: 'linear'
	
      });

if (window.matchMedia("(max-width: 768px)").matches) {

      $('.home-damen').slick({
        accessibility: true,
		arrows: true,
        nextArrow: '<div class="products-container-slider-arrow products-container-slider-arrow-right"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/arrow-product-next.png"></div>',
  		prevArrow: '<div class="products-container-slider-arrow products-container-slider-arrow-left"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/arrow-product-prev.png"></div>',
        autoplay: true,
        slidesToScroll: 1,
  		autoplaySpeed: 3000,
        infinite: true,
        speed: 700,
        fade: true,
        cssEase: 'linear'
	
      });

}


if (window.matchMedia("(max-width: 768px)").matches) {

$('.home-herren').slick({
  accessibility: true,
  arrows: true,
  nextArrow: '<div class="products-container-slider-arrow products-container-slider-arrow-right"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/arrow-product-next.png"></div>',
    prevArrow: '<div class="products-container-slider-arrow products-container-slider-arrow-left"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/arrow-product-prev.png"></div>',
  autoplay: true,
  slidesToScroll: 1,
    autoplaySpeed: 3000,
  infinite: true,
  speed: 700,
  fade: true,
  cssEase: 'linear'

});

}

		
    });


   
  </script>


</html>
</html>
