<?php
/**
 * Capranea functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Capranea
 */


if ( ! function_exists( 'capranea_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function capranea_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Capranea, use a find and replace
		 * to change 'capranea' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'capranea', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'capranea' ),
			'responsive' => esc_html__( 'Responsive', 'capranea' ),
			'footer' => esc_html__( 'Footer', 'capranea' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'capranea_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'capranea_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function capranea_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'capranea_content_width', 640 );
}
add_action( 'after_setup_theme', 'capranea_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function capranea_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'capranea' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'capranea' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer', 'capranea' ),
		'id'            => 'footer-1',
		'description'   => esc_html__( 'Add widgets here.', 'capranea' ),
		'before_widget' => '<section id="%1$s" class="footer %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'capranea_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function capranea_scripts() {
	wp_enqueue_style( 'capranea-style', get_stylesheet_uri() );

	wp_enqueue_script( 'capranea-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'capranea-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'capranea_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce.php';
}


/* Force login if retail section */


function wooc_extra_register_fields() {?>
	<p class="form-row form-row-wide">
		<label for="reg_billing_phone"><?php _e( 'Username', 'woocommerce' ); ?></label>
		<input type="text" class="woocommerce-Input woocommerce-Input--text input-text loginpage-text-field login-form-field" name="billing_phone" id="reg_billing_phone" value="<?php esc_attr_e( $_POST['billing_phone'] ); ?>" />
	</p>
	<p class="form-row form-row-wide">
		<label for="reg_billing_phone"><?php _e( 'Post Code', 'woocommerce' ); ?></label>
		<input type="text" class="woocommerce-Input woocommerce-Input--text input-text loginpage-text-field login-form-field" name="billing_phone" id="reg_billing_phone" value="<?php esc_attr_e( $_POST['billing_phone'] ); ?>" />
	</p>
	<p class="form-row form-row-first">
		<label for="reg_billing_first_name"><?php _e( 'Country', 'woocommerce' ); ?><span class="required">*</span></label>
		<input type="text" class="woocommerce-Input woocommerce-Input--text input-text loginpage-text-field login-form-field" name="billing_first_name" id="reg_billing_first_name" value="<?php if ( ! empty( $_POST['billing_first_name'] ) ) esc_attr_e( $_POST['billing_first_name'] ); ?>" />
	</p>

	
	<div class="clear"></div>
	<?php
}
add_action( 'woocommerce_register_form_start', 'wooc_extra_register_fields' );

function wooc_validate_extra_register_fields( $username, $email, $validation_errors ) {
 
	if ( isset( $_POST['billing_first_name'] ) && empty( $_POST['billing_first_name'] ) ) {

		   $validation_errors->add( 'billing_first_name_error', __( '<strong>Error</strong>: First name is required!', 'woocommerce' ) );

	}

	if ( isset( $_POST['billing_last_name'] ) && empty( $_POST['billing_last_name'] ) ) {

		   $validation_errors->add( 'billing_last_name_error', __( '<strong>Error</strong>: Last name is required!.', 'woocommerce' ) );

	}
	   return $validation_errors;
}

add_action( 'woocommerce_register_post', 'wooc_validate_extra_register_fields', 10, 3 );

function wooc_save_extra_register_fields( $customer_id ) {
    if ( isset( $_POST['billing_phone'] ) ) {
                 // Phone input filed which is used in WooCommerce
                 update_user_meta( $customer_id, 'billing_phone', sanitize_text_field( $_POST['billing_phone'] ) );
          }
      if ( isset( $_POST['billing_first_name'] ) ) {
             //First name field which is by default
             update_user_meta( $customer_id, 'first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
             // First name field which is used in WooCommerce
             update_user_meta( $customer_id, 'billing_first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
      }
      if ( isset( $_POST['billing_last_name'] ) ) {
             // Last name field which is by default
             update_user_meta( $customer_id, 'last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
             // Last name field which is used in WooCommerce
             update_user_meta( $customer_id, 'billing_last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
      }
 
}
add_action( 'woocommerce_created_customer', 'wooc_save_extra_register_fields' );



add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );
  

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);

function remove_dashboard_widgets () {

	remove_meta_box('dashboard_quick_press','dashboard','side'); //Quick Press widget
	remove_meta_box('dashboard_recent_drafts','dashboard','side'); //Recent Drafts
	remove_meta_box('dashboard_primary','dashboard','side'); //WordPress.com Blog
	remove_meta_box('dashboard_secondary','dashboard','side'); //Other WordPress News
	remove_meta_box('dashboard_incoming_links','dashboard','normal'); //Incoming Links
	remove_meta_box('dashboard_plugins','dashboard','normal'); //Plugins
	remove_meta_box('dashboard_right_now','dashboard', 'normal'); //Right Now
	remove_meta_box('rg_forms_dashboard','dashboard','normal'); //Gravity Forms
	remove_meta_box('dashboard_recent_comments','dashboard','normal'); //Recent Comments
	remove_meta_box('icl_dashboard_widget','dashboard','normal'); //Multi Language Plugin
	remove_meta_box('dashboard_activity','dashboard', 'normal'); //Activity
	remove_action('welcome_panel','wp_welcome_panel');
  
  }
  
  add_action('wp_dashboard_setup', 'remove_dashboard_widgets');


  add_filter('woocommerce_swatches_get_swatch_anchor_css_class', 'add_swatch_out_stock_class', 10, 2);

  function add_swatch_out_stock_class( $anchor_classes, $swatch_term ) {
	  if ( is_product() ) {
		  global $post;
		  $product = wc_get_product($post);
  
		  if ( $product->get_type() === 'variable' ) {
			  foreach( $product->get_available_variations() as $variation ) {
				  $product_variation = new WC_Product_Variation($variation['variation_id']);
  
				  if( $product_variation->get_stock_quantity() === 0 ) {
					  foreach( $product_variation->get_variation_attributes() as $var_attribute) {
						  if( $swatch_term->term_slug === $var_attribute) {
							  $anchor_classes .= ' out-of-stock';
						  }
					  }
				  }
			  }
		  }
	  }
	  return $anchor_classes;
  }


  add_filter( 'woocommerce_product_tabs', 'bbloomer_remove_product_tabs', 98 );
 
function bbloomer_remove_product_tabs( $tabs ) {
    unset( $tabs['additional_information'] ); 
    return $tabs;
}


function my_login_logo() { ?>
    <style type="text/css">
        body  {
            background-image: url('https://mestech.co/wp-content/themes/capranea/assets/images/background-slider-1.jpg') !important;
		}
		
		#wp-submit {
			color: #777 !important;
		background: #e5e5e5 !important;

		-webkit-box-sizing: border-box;
		-moz-box-sizing: border-box;
		box-sizing: border-box;
		padding: 4px 13px;
		margin: 0 5px;
	
		border: 2px solid transparent;
		font-size: 11px;
		font-weight: 400;
		text-transform: uppercase;
		text-decoration: none;
		letter-spacing: 2px;
		-webkit-border-radius: 0;
		-moz-border-radius: 0;
		border-radius: 0;
		-webkit-box-shadow: none !important;
		-moz-box-shadow: none !important;
		box-shadow: none !important;
		-webkit-transition: all 0.2s cubic-bezier(0.000, 0.000, 0.580, 1.000);
		-moz-transition: all 0.2s cubic-bezier(0.000, 0.000, 0.580, 1.000);
		-o-transition: all 0.2s cubic-bezier(0.000, 0.000, 0.580, 1.000);
		-ms-transition: all 0.2s cubic-bezier(0.000, 0.000, 0.580, 1.000);
		transition: all 0.2s cubic-bezier(0.000, 0.000, 0.580, 1.000);

		display: inline-block;
		margin-bottom: 0;
		font-weight: normal;
		text-align: center;
		vertical-align: middle;
		-ms-touch-action: manipulation;
		touch-action: manipulation;
		cursor: pointer;
		background-image: none;
		border: 1px solid transparent;
		white-space: nowrap;
		padding: 6px 12px;
		font-size: 14px;
		line-height: 1.42857143;
		-webkit-user-select: none;
		-moz-user-select: none;
		-ms-user-select: none;
		user-select: none;
		}

		.wp-core-ui .button-primary {
 
			text-decoration: none !important;
			text-shadow: none !important;
		}
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

 remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display' );


 add_action( 'woocommerce_variation_options_pricing', 'bbloomer_add_custom_field_to_variations', 10, 3 ); 
 


 
function variation_settings_fields( $loop, $variation_data, $variation ) {

    // Text Field
    woocommerce_wp_text_input( 
        array( 
            'id'          => '_gtin_code[' . $variation->ID . ']', 
            'label'       => __( 'GTIN-CODE', 'woocommerce' ), 
            'placeholder' => 'GTIN-CODE',
            'desc_tip'    => 'true',
            'description' => __( 'Enter your custom GTIN code.', 'woocommerce' ),
            'value'       => get_post_meta( $variation->ID, '_gtin_code', true )
        )
    );
    woocommerce_wp_text_input( 
        array( 
            'id'          => '_stock_1' . $variation->ID . ']', 
            'label'       => __( 'Enter nr. of Stock in the first warehouse', 'woocommerce' ), 
            'desc_tip'    => 'true',
            'description' => __( 'Enter nr. of Stock in the first warehouse', 'woocommerce' ),
            'value'       => get_post_meta( $variation->ID, '_stock_1', true ),
            'custom_attributes' => array(
                            'step'  => 'any',
                            'min'   => '0'
                        ) 
        )
    );
    woocommerce_wp_text_input( 
        array( 
            'id'          => '_stock_2[' . $variation->ID . ']', 
            'label'       => __( 'Nr. of Stock in second warehouse', 'woocommerce' ), 
            'desc_tip'    => 'true',
            'description' => __( 'Nr. of Stock in second warehouse', 'woocommerce' ),
            'value'       => get_post_meta( $variation->ID, '_stock_2', true ),
            'custom_attributes' => array(
                            'step'  => 'any',
                            'min'   => '0'
                        ) 
        )
    );
  
}

/**
 * Save new fields for variations
 *
 */
function save_variation_settings_fields( $post_id ) {

    // Text Field
    $text_field = $_POST['_gtin_code'][ $post_id ];
    if( ! empty( $text_field ) ) {
        update_post_meta( $post_id, '_gtin_code', esc_attr( $text_field ) );
    }
    $number_field = $_POST['_stock_1'][ $post_id ];
    if( ! empty( $number_field ) ) {
        update_post_meta( $post_id, '_stock_1', esc_attr( $number_field ) );
    }
    $number_field = $_POST['_stock_2'][ $post_id ];
    if( ! empty( $number_field ) ) {
        update_post_meta( $post_id, '_stock_2', esc_attr( $number_field ) );
    }
  

}

// Save Variation Settings
add_action( 'woocommerce_save_product_variation', 'save_variation_settings_fields', 10, 2 );


// Add Variation Settings
add_action( 'woocommerce_product_after_variable_attributes', 'variation_settings_fields', 10, 3 );


//displays all users on the wp-rest api not just those that published a pos
class UserFields {

	function __construct() {
	 add_filter('rest_user_query',           [$this, 'show_all_users']);
	}
   
   function show_all_users($prepared_args) {
	   unset($prepared_args['has_published_posts']);
   
	   return $prepared_args;
	 }
   }
   
   new UserFields();


   add_filter('woocommerce_variable_price_html', 'custom_variation_price', 10, 2);
function custom_variation_price( $price, $product ) {
    $available_variations = $product->get_available_variations();
    $selectedPrice = '';
    $dump = '';

    foreach ( $available_variations as $variation )
    {
        // $dump = $dump . '<pre>' . var_export($variation['attributes'], true) . '</pre>';

        $isDefVariation=false;
        foreach($product->get_default_attributes() as $key=>$val){
            // $dump = $dump . '<pre>' . var_export($key, true) . '</pre>';
            // $dump = $dump . '<pre>' . var_export($val, true) . '</pre>';
            if($variation['attributes']['attribute_'.$key]==$val){
                $isDefVariation=true;
            }   
        }
        if($isDefVariation){
            $price = $variation['display_price'];         
        }
    }
    $selectedPrice = wc_price($price);

//  $dump = $dump . '<pre>' . var_export($available_variations, true) . '</pre>';

    return $selectedPrice . $dump;
}


add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );

function new_loop_shop_per_page( $cols ) {
  // $cols contains the current number of products per page based on the value stored on Options -> Reading
  // Return the number of products you wanna show per page.
  $cols = 24;
  return $cols;

}

add_filter('woocommerce_currency_symbol', 'change_existing_currency_symbol', 10, 2);

function change_existing_currency_symbol( $currency_symbol, $currency ) {
     switch( $currency ) {
          case 'CHF': $currency_symbol = 'CHF '; break;
     }
     return $currency_symbol;
}

add_filter('woocommerce_show_variation_price',      function() { return TRUE;});


remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);

add_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 6);



function custom_excerpt_length( $length ) {
	return 35;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


add_theme_support( 'post-formats', array( 'aside', 'gallery' ) );




if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array(
		'key' => 'group_5ba81385da84a',
		'title' => 'Home Page',
		'fields' => array(
			array(
				'key' => 'field_5bab6b160159a',
				'label' => 'Slider - Slide 1 - Header',
				'name' => 'slider-slide_1-header',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array(
				'key' => 'field_5bab6b350159b',
				'label' => 'Slider - Slide 1 - Header 2',
				'name' => 'slider-slide_1-header2',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array(
				'key' => 'field_5bab6b3d0159c',
				'label' => 'Slider - Slide 1 - Link Text',
				'name' => 'slider-slide_1-linkText',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array(
				'key' => 'field_5bab6b6c0159d',
				'label' => 'Slider - Slide 1 - Link Direction',
				'name' => 'slider-slide_1-link-direction',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array(
				'key' => 'field_5bab6b8b0159f',
				'label' => 'Slider - Slide 1 - Link Text 2',
				'name' => 'slider-slide_1-linkText2',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array(
				'key' => 'field_5bab6ba7015a0',
				'label' => 'Slider - Slide 1 - Link Direction 2',
				'name' => 'slider-slide_1-link-direction2',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array(
				'key' => 'field_5bab6c50e22df',
				'label' => 'Slider - Slide 2 - Header',
				'name' => 'slider-slide_2-header',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array(
				'key' => 'field_5bab6c77e22e1',
				'label' => 'Slider - Slide 2 - Header 2',
				'name' => 'slider-slide_2-header2',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array(
				'key' => 'field_5bab6c98e22e3',
				'label' => 'Slider - Slide 2 - Link Text',
				'name' => 'slider-slide_2-linkText',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array(
				'key' => 'field_5bab6cb1e22e4',
				'label' => 'Slider - Slide 2 - Link Direction',
				'name' => 'slider-slide_2-linkDirection',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array(
				'key' => 'field_5ba8138ff8eb0',
				'label' => 'Lookbook Intro',
				'name' => 'lookbook_intro',
				'type' => 'textarea',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => '',
				'new_lines' => '',
			),
			array(
				'key' => 'field_5bbce0785b335',
				'label' => 'Leave Your Mark - Text',
				'name' => 'leaveyourmarktext',
				'type' => 'textarea',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => '',
				'new_lines' => '',
			),
			array(
				'key' => 'field_5bbce182b784e',
				'label' => 'Leave Your Mark - Link',
				'name' => 'leaveyourmarklink',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array(
				'key' => 'field_5bbce196b784f',
				'label' => 'Leave Your Mark - Link Text',
				'name' => 'leaveyourmark-linktext',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'home-page.php',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));
	
	acf_add_local_field_group(array(
		'key' => 'group_5ba812ca685ce',
		'title' => 'Lookbook',
		'fields' => array(
			array(
				'key' => 'field_5ba812ce8ba94',
				'label' => 'Image',
				'name' => 'lookbook-image',
				'type' => 'image',
				'instructions' => '',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'return_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
				'min_width' => '',
				'min_height' => '',
				'min_size' => '',
				'max_width' => '',
				'max_height' => '',
				'max_size' => '',
				'mime_types' => '',
			),
			array(
				'key' => 'field_5ba812fb8ba95',
				'label' => 'Description',
				'name' => 'lookbook-description',
				'type' => 'textarea',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => '',
				'new_lines' => '',
			),
			array(
				'key' => 'field_5ba813118ba96',
				'label' => 'FULL Related product 1 SKU (Style no)',
				'name' => 'related_product_1_sku',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array(
				'key' => 'field_5ba813458ba98',
				'label' => 'FULL Related product 2 SKU (Style no)',
				'name' => 'related_product_2_sku',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array(
				'key' => 'field_5ba813598ba9a',
				'label' => 'FULL Related product 3 SKU (Style no)',
				'name' => 'related_product_3_sku',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'lookbook',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));
	
	acf_add_local_field_group(array(
		'key' => 'group_5ba15c365d9fc',
		'title' => 'Products',
		'fields' => array(
			array(
				'key' => 'field_5b9d5e72efe18',
				'label' => 'FUTTERUNG',
				'name' => 'futterung',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array(
				'key' => 'field_5b9d5e77efe19',
				'label' => 'ISOLIERUNG',
				'name' => 'isolierung',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array(
				'key' => 'field_5b9d5e81efe1a',
				'label' => 'MATERIAL',
				'name' => 'material',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'product',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'seamless',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));
	
	acf_add_local_field_group(array(
		'key' => 'group_5ba5c561bf2ce',
		'title' => 'Slider',
		'fields' => array(
			array(
				'key' => 'field_5ba5c5e91371e',
				'label' => 'Image Slider 1',
				'name' => 'image_slider_1',
				'type' => 'image',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'return_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
				'min_width' => '',
				'min_height' => '',
				'min_size' => '',
				'max_width' => '',
				'max_height' => '',
				'max_size' => '',
				'mime_types' => '',
			),
			array(
				'key' => 'field_5ba5c6091371f',
				'label' => 'Image Slider 2',
				'name' => 'image_slider_2',
				'type' => 'image',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => array(
					array(
						array(
							'field' => 'field_5ba5c5e91371e',
							'operator' => '!=empty',
						),
					),
				),
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'return_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
				'min_width' => '',
				'min_height' => '',
				'min_size' => '',
				'max_width' => '',
				'max_height' => '',
				'max_size' => '',
				'mime_types' => '',
			),
			array(
				'key' => 'field_5ba5c61713720',
				'label' => 'Image Slider 3',
				'name' => 'image_slider_3',
				'type' => 'image',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => array(
					array(
						array(
							'field' => 'field_5ba5c6091371f',
							'operator' => '!=empty',
						),
					),
				),
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'return_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
				'min_width' => '',
				'min_height' => '',
				'min_size' => '',
				'max_width' => '',
				'max_height' => '',
				'max_size' => '',
				'mime_types' => '',
			),
			array(
				'key' => 'field_5ba5c63f13721',
				'label' => 'image_slider_4',
				'name' => 'image_slider_4',
				'type' => 'image',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => array(
					array(
						array(
							'field' => 'field_5ba5c61713720',
							'operator' => '!=empty',
						),
					),
				),
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'return_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
				'min_width' => '',
				'min_height' => '',
				'min_size' => '',
				'max_width' => '',
				'max_height' => '',
				'max_size' => '',
				'mime_types' => '',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'post',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));
	
	acf_add_local_field_group(array(
		'key' => 'group_5ba03a49104ed',
		'title' => 'Stores',
		'fields' => array(
			array(
				'key' => 'field_5b99967457c1e',
				'label' => 'Strasse',
				'name' => 'storefinder-strasse',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array(
				'key' => 'field_5b99968257c1f',
				'label' => 'Ort',
				'name' => 'storefinder-ort',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array(
				'key' => 'field_5b99968f57c20',
				'label' => 'PLZ',
				'name' => 'storefinder-plz',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array(
				'key' => 'field_5b99969a57c21',
				'label' => 'LKZ',
				'name' => 'storefinder-lkz',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array(
				'key' => 'field_5b9996a957c22',
				'label' => 'Homepage',
				'name' => 'storefinder-homepage',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'stores',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'seamless',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => array(
		),
		'active' => 1,
		'description' => '',
	));
	
	endif;



	/* added after going live - make sure to merge */

	remove_filter( 'authenticate', 'wp_authenticate_username_password' );
	add_filter( 'authenticate', 'wpse_115539_authenticate_username_password', 20, 3 );
	/**
	 * Remove Wordpress filer and write our own with changed error text.
	 */

	function wpse_115539_authenticate_username_password( $user, $username, $password ) {
		if ( is_a($user, 'WP_User') )
			return $user;
	
		if ( empty( $username ) || empty( $password ) ) {
			if ( is_wp_error( $user ) )
				return $user;
	
			$error = new WP_Error();
	
			if ( empty( $username ) )
				$error->add( 'empty_username', __('<strong>ERROR</strong>: The username field is empty.' ) );
	
			if ( empty( $password ) )
				$error->add( 'empty_password', __( '<strong>ERROR</strong>: The password field is empty.' ) );
	
			return $error;
		}
	
		$user = get_user_by( 'login', $username );
	
		if ( !$user )
			return new WP_Error( 'invalid_username', sprintf( __( 'ERROR: Invalid username. Lost your password?' ), wp_lostpassword_url() ) );
	
		$user = apply_filters( 'wp_authenticate_user', $user, $password );
		if ( is_wp_error( $user ) )
			return $user;
	
		if ( ! wp_check_password( $password, $user->user_pass, $user->ID ) )
			return new WP_Error( 'incorrect_password', sprintf( __( 'ERROR: The password you entered for the username %1$s is incorrect. ?' ),
			$username, wp_lostpassword_url() ) );
	
		return $user;
	}