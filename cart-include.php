<div class="cart-widget">
	<div class="cart-widget-info">
	<?php if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
 
 $count = WC()->cart->cart_contents_count;
 ?><a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><?php 
 if ( $count > 0 ) {
	 ?>
	 <span class="cart-contents-count"><?php echo esc_html( $count ); ?></span>
	 <?php
 }
	 ?></a>

<?php } ?>
	</div>
	<div class="cartwidget-icon-product-page">
		<a  href="<?php echo WC()->cart->get_cart_url();?>"><img src="https://capranea.com/finalstaging/wp-content/themes/capranea/assets/cart/cart_solo.png"></a>
	</div>

</div>