<?php /* Template Name: Home Page */ 


get_header();
?>


<div class="slider-container">

    <div class="home-slide second-slide">
        <div class="second-slide-content home-slide-content">
            <h2><?php the_field('slider-slide_1-header'); ?></h2>
            <h1> <?php the_field('slider-slide_1-header2'); ?></h1>
            <div>
                <h3>
                    <a href="<?php the_field('slider-slide_1-link-direction'); ?>"><?php the_field('slider-slide_1-linkText'); ?></a>
                    <a>|</a>
                    <a href="<?php the_field('slider-slide_1-link-direction2'); ?>"><?php the_field('slider-slide_1-linkText2'); ?></a>
                </h3>
            </div>
        </div>
    </div>

    <div class="home-slide first-slide">
        <div class="second-slide-content home-slide-content"">
            <h2><?php the_field('slider-slide_2-header'); ?> </h2>
            <h1> <?php the_field('slider-slide_2-header2'); ?></h1>

             <div>
                <h3>
                    <a href="<?php the_field('slider-slide_2-linkDirection'); ?>"><?php the_field('slider-slide_2-linkText'); ?></a>

                </h3>
            </div>
        </div>
    </div>

    

</div>








<div class="bestseller">
    <div class="bestseller-title">

        	<?php 
 
  switch ($blog_id) {

    case "1":
        echo '<h2>BESTSELLER DAMEN</h2>';
        echo '<a href="https://capranea.com/finalstaging/category/ladies/"><h3>ZUR DAMEN-KOLLEKTION <span>&#8250;</span></h3></a>';
    break;
    case "2":
        echo '<h2>BESTSELLER LADIES</h2>';
        echo '<a href="https://capranea.com/finalstaging/en/product-category/ladies/"><h3>TO THE LADIES COLLECTION <span>&#8250;</span></h3></a>';
    break;
    case "3":
        echo '<h2>BESTSELLER LADIES</h2>';
        echo '<a href="https://capranea.com/finalstaging/category/ladies/"><h3>TO THE LADIES COLLELCTION <span>&#8250;</span></h3></a>';
    break;
   
    
     
     
  };
 ?>


        
        
    </div>

<?php restore_current_blog(); //switched back to main site (check above ?>


    <div class="products-container home-damen">

            <?php
            $args = array( 'post_type' => 'product', 'posts_per_page' => 4, 'product_cat' => 'ladies', 'orderby' => 'rand' );
            $loop = new WP_Query( $args );
            while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>
                <div class="products-content">
                        <a href="<?php echo get_permalink( $loop->post->ID ) ?>" title="<?php echo esc_attr($loop->post->post_title ? $loop->post->post_title : $loop->post->ID); ?>">
                            <?php woocommerce_show_product_sale_flash( $post, $product ); ?>
                            <?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, 'shop_catalog'); else echo '<img src="'.woocommerce_placeholder_img_src().'" alt="Placeholder" width="300px" height="300px" />'; ?>
                            <h4><?php the_title(); ?></h4>
                            <div>
                                <p><?php echo $product->get_price_html(); ?></p>
                            </div>    
                
                        </a>
                        
                </div>
        <?php endwhile; ?>
        <?php wp_reset_query(); ?>
        
      
    </div>





    <div class="bestseller-title">
            <?php 
            
            switch ($blog_id) {

            case "1":
                echo '<h2>BESTSELLER HERREN</h2>';
                echo '        <a href="https://capranea.com/finalstaging/category/men/"><h3>ZUR HERREN-KOLLEKTION <span>&#8250;</span></h3></a>';
            break;
            case "2":
                echo '<h2>BESTSELLER MEN</h2>';
                echo '<a href="https://capranea.com/finalstaging/en/product-category/men/"><h3>TO THE MEN';
                echo "'";
                echo '<span>&#8250;</span></h3></a>';
            break;
            case "3":
                echo '<h2>BESTSELLER MEN</h2>';
                echo '<a href="https://capranea.com/finalstaging/category/ladies/"><h3>TO THE MEN';
                echo "'S";
                echo ' COLLECTION<span>&#8250;</span></h3></a>';
            break;
            
            
                
                
            };
            ?>
       
    </div>

    <div class="products-container home-herren">
    <?php
            $args = array( 'post_type' => 'product', 'posts_per_page' => 4, 'product_cat' => 'men', 'orderby' => 'rand' );
            $loop = new WP_Query( $args );
            while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>
                <div class="products-content">
                        <a href="<?php echo get_permalink( $loop->post->ID ) ?>" title="<?php echo esc_attr($loop->post->post_title ? $loop->post->post_title : $loop->post->ID); ?>">
                            <?php woocommerce_show_product_sale_flash( $post, $product ); ?>
                            <?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, 'shop_catalog'); else echo '<img src="'.woocommerce_placeholder_img_src().'" alt="Placeholder" width="300px" height="300px" />'; ?>
                            <h4><?php the_title(); ?></h4>
                            <div>
                                <p><?php echo $product->get_price_html(); ?></p>
                            </div>  
  
                        </a>

                        
                </div>
        <?php endwhile; ?>
        <?php wp_reset_query(); ?>

      
        
    </div>

   		
    </div>
</div>



<!-- LOOKBOOK --> 


<?php 

if ($blog_id != 1) {
    global $switched;
    switch_to_blog(1);
}

/* switching the loop to the main site for the lookbook to come from one source */  
?>


<?php
    $countercss = 0;
    $loop = new WP_Query( array( 'post_type' => 'lookbook', 'order' => 'ASC' ) );
    if ( $loop->have_posts() ) :
        while ( $loop->have_posts() ) : $loop->the_post(); ?>
           <?php $countercss++; ?>
            <style>
                .lookbook-slider-slide<?php echo $countercss ?>bg {
                    background-image: url(<?php the_field('lookbook-image'); ?>);
                }
            </style>
          
        <?php endwhile;
        
    endif;
    wp_reset_postdata();

   
?>

<?php restore_current_blog(); //switched back to main site (check above ?>






<div id="lookbook-container" class="lookbook-container">
    <div class="lookbook">
        <div id="lookbook-text">			
            <h4>LOOKBOOK</h4>
            <p><?php the_field('lookbook_intro'); ?></p>
            <p><a  target="_blank" href="https://capranea.com/finalstaging/wp-content/uploads/2018/10/lookbook.pdf">DOWNLOAD LOOKBOOK 18/19<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/download.png"></a> </p>
        </div>  
    </div>

<?php 

if ($blog_id != 1) {
    global $switched;
    switch_to_blog(1);
}

/* switching the loop to the main site for the lookbook to come from one source */  
?>
    <?php
    $counter = 0;
    $loop = new WP_Query( array( 'post_type' => 'lookbook', 'order' => 'ASC' ) );
    if ( $loop->have_posts() ) :
        while ( $loop->have_posts() ) : $loop->the_post(); ?>
           <?php $counter++; ?>
           <div v-if="lookbookitem === <?php echo $counter; ?>" v-on:mousemove="makearrowsvisible()" id="lookbook-start" class="lookbook lookbook-slider lookbook-slider-slide<?php echo $counter; ?>bg" > 
                <img class="left" v-if="arrowsvisible === true" v-on:click="left()" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/arrow-lookbook-prev.svg">
                <img class="right"v-if="arrowsvisible === true" v-on:click="right()" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/arrow-lookbook-next.svg">
                <img class="previemodebutton" v-if="arrowsvisible === true" v-on:click="activatepreviewmode()" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/see-more-lookbook.svg">
            </div>
          
        <?php endwhile;
        
    endif;
    wp_reset_postdata();

   
?>


   


  

    <div v-if="previewmode === true" class="shadowlookbook">
        <div class="related-products related-products-container">
            <div class="title">
                    <h2>RELATED PRODUCTS</h2>
            </div>



    <?php
    $countershadow = 0;
    $loop = new WP_Query( array( 'post_type' => 'lookbook', 'order' => 'ASC' ) );
    if ( $loop->have_posts() ) :
        while ( $loop->have_posts() ) : $loop->the_post(); ?>
           <?php $countershadow++; ?>
          
           <div v-if="lookbookitem === <?php echo $countershadow; ?>" >
                <div class="products-container">
                        <div class="products-content"> 

                      
                            <?php 
                                    $sku1 = get_field('related_product_1_sku'); 
                                    $sku2 = get_field('related_product_2_sku'); 
                                    $sku3 = get_field('related_product_3_sku'); 
                                    restore_current_blog(); //switched back to main site (check above 

                                    $sku1Trimmed = substr($sku1, 0, -6); 
                                    $lookbook_parent_id = wc_get_product_id_by_sku($sku1Trimmed);
                                    $lookbook_variation_id = wc_get_product_id_by_sku($sku1);
                                    $post   = get_post($lookbook_parent_id);
                                    $title = $post->post_title;
                                    $product_variation = new WC_Product_Variation( $lookbook_variation_id );
                                        $variation_image = $product_variation->get_image();
                                        $variation_price = $product_variation->get_price_html();
                                        $variation_url = $product_variation->get_permalink();
                                        $variation_color = $product_variation->get_attribute('colors');
                                ?>

                                <a href="<?php echo $variation_url; ?> ">
                                    <?php  echo $variation_image; ?>
                                    <h4><?php  echo $title; ?></h4>
                                    <h5><?php echo $variation_color; ?></h5>
                                    <div>
                                        <p> <?php echo $variation_price; ?></p>
                                    </div>
                                </a>
                        </div>


                       
                        

                        <div class="products-content">
                            <?php 

                                $sku2Trimmed = substr($sku2, 0, -6); 
                                $lookbook_parent_id2 = wc_get_product_id_by_sku($sku2Trimmed);
                                $lookbook_variation_id2 = wc_get_product_id_by_sku($sku2);
                                $post2   = get_post($lookbook_parent_id2);
                                $title2 = $post2->post_title;

                                $product_variation2 = new WC_Product_Variation( $lookbook_variation_id2 );
                                        $variation_image2 = $product_variation2->get_image();

                                        $variation_price2 = $product_variation2->get_price_html();
                                        $variation_url2 = $product_variation2->get_permalink();
                                        $variation_color2 = $product_variation2->get_attribute('colors');
                                    
                            ?>
                                <a href="<?php echo $variation_url2; ?> ">

                                <?php  echo $variation_image2; ?>

                                    <h4><?php  echo $title2; ?></h4>
                                
                                


                                    <h5><?php echo $variation_color2; ?></h5>
                                    <div>
                                        <p> <?php echo $variation_price2; ?></p>
                                    </div>
                                    </a>
                        </div>

                         <?php if($sku3) {
                            /* include third product for the lookbook if it exists */
                            include ('wp-content/themes/capranea/inc/lookbook-third.php');

                        } 
                        ?>
                        
                        <?php 

if ($blog_id != 1) {
    global $switched;
    switch_to_blog(1);
}

/* switching the loop to the main site for the lookbook to come from one source */  
?>
      

                         
                            
                         
                
                    </div>
    </div>
                    <?php endwhile;
        
    endif;
    wp_reset_postdata();

   
?>
</div> 
       
        
    <div class="slider lookbook-text-description-container">
                            <img class="closepreviemodebutton"  v-on:click="deactivatepreviewmode()" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/cross-final.png">
                            <?php
                                $counterdesc = 0;
                                $loop = new WP_Query( array( 'post_type' => 'lookbook', 'order' => 'ASC' ) );
                                if ( $loop->have_posts() ) :
                                    while ( $loop->have_posts() ) : $loop->the_post(); ?>
                                    <?php $counterdesc++; ?>
                                      
                                         <div v-if="lookbookitem === <?php echo $counterdesc ?>" >
                                            <div class="lookbook-text-description">			
                                                <p><?php the_field('lookbook-description'); ?></p>                    
                                            </div>      
                                        </div>
                                    
                                    <?php endwhile;
                                    
                                endif;
                                wp_reset_postdata();

                            
                            ?>

          
        </div>
    </div>
</div>

<?php restore_current_blog(); //switched back to main site (check above ?>



<!-- END LOOKBOOK  --> 



<div class="highlights">
   <!-- <div class="highlights-title">
        <h2>HÖHEPUNKT</h2>
        <a href=""><h3>ZUR HÖHEPUNKT PRODUKTE<span>&#8250;</span></h3></a>
    </div>

    <div class="highlights-container">
        <div class="highlights-content">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/highlights-products/highlight-product-1.png">
            <h4>CLOUD</h4>
            <h5>WHITE</h5>
            <div>
                <p>999 CHF</p>
            </div>
        </div>

        <div class="highlights-content">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/highlights-products/highlight-product-2.png">
            <h4>MAX</h4>
            <h5>1-OF-KIND BLACK</h5>
            <div>
                <p>1299 CHF</p>
            </div>
        </div>
    </div>

    <div class="responsive-highlights-container">
        <div class="highlights-content">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/highlights-products/highlight-product-1.png">
            <h4>CLOUD</h4>
            <h5>WHITE</h5>
            <div>
                <p>999 CHF</p>
            </div>
        </div>
        
        <img class="products-nav" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/arrow-product-prev.svg">
        <img class="products-nav prod-nav-next" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/arrow-product-next.svg">			
    </div> -->
</div>

<div class="about-container">
    <div class="about-overlay"></div>
    <div class="about-text">
        <h3>CAPRANEA</h3>
        <h4>LEAVE YOUR MARK</h4> 

        <p><?php the_field('leaveyourmarktext'); ?></p>
        <h4><a href="<?php the_field('leaveyourmarklink');?>"><?php the_field('leaveyourmark-linktext');?> <img id="about-link-arrow" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/link-arrow-fff.svg"></a></h4>
    </div>
    <img class="story-image" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/about-img.png">
</div>

<div class="responsive-about-container">
    <div class="about-overlay"></div>
    <div class="responsive-about-content">
        <img class="about-img" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/about-img.png">
        <div class="about-text">
            <h3>CAPRANEA</h3>
            <h4>LEAVE YOUR MARK</h4>
            <p><?php the_field('leaveyourmarktext'); ?></p>           
            <h4> <a href="<?php the_field('leaveyourmarklink');?>"><?php the_field('leaveyourmark-linktext');?> <img id="about-link-arrow" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/link-arrow-fff.svg"></a></h4>
        </div>
    </div>
</div>

<div class="latest-news">
    <div class="latest-news-title">
        <h2>LATEST NEWS</h2>

        <?php 
 
 switch ($blog_id) {

   case "1":
       echo '        <a href="https://capranea.com/finalstaging/blog/category/blog/"><h3>ALLE BEITRÄGE IN UNSEREM BLOG<span>&#8250;</span></h3></a>';
   break;
   case "2":
      
       echo '        <a href="https://capranea.com/finalstaging/en/blog/category/blog/"><h3>ALL THE LATETS POSTS IN OUR BLOG<span>&#8250;</span></h3></a>';
   break;
   case "3":
      
       echo '<a href="https://capranea.com/finalstaging/retail/category/blog/"><h3>ALL THE LATETS POSTS IN OUR BLOG<span>&#8250;</span></h3></a>';
   break;
  
   
    
    
 };
?>

    </div>
    <div class="latest-news-container">

    <?php
        $args = array(
            'cat' => 89,
            'posts_per_page'  => 3
        );
       query_posts($args); 
     ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <div class="latest-news-content">
          <a href="<?php the_permalink(); ?>">  <?php the_post_thumbnail(  'post-thumbnail'); ?>
            <div class="latest-news-text">
                <h5><?php the_title(); ?> </h5>
    <p> <?php the_excerpt(); ?></p>
    <h5>MEHR ERFAHREN <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/link-arrow-000.svg"></h5>
</a>
    </div>
</div>

<?php endwhile; endif; ?>
</div>
<?php wp_reset_query(); ?>

    

      
    
</div>





<?php
get_footer();
