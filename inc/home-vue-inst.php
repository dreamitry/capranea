<?php 

if ($blog_id != 1) {
    global $switched;
    switch_to_blog(1);
}

/* switching the loop to the main site for the lookbook to come from one source */  
?>
   <?php
    $countervue = 0;
    $loop = new WP_Query( array( 'post_type' => 'lookbook') );
    if ( $loop->have_posts() ) :
        while ( $loop->have_posts() ) : $loop->the_post(); ?>
           <?php $countervue++; ?>
          
        <?php endwhile;
        
    endif;
    wp_reset_postdata();
	$counterviewminus = $countervue--;

   
?>
<?php restore_current_blog(); //switched back to main site (check above ?>


<script>
new Vue({	
el: '.lookbook-container',
data: {
	lookbookitem: 1,
	previewmode: false,
	arrowsvisible: true,
},

methods: { 
	left: function() {	
		if (this.lookbookitem === 1 ) {
			
			<?php 
						for ($x = 1; $x < $counterviewminus; $x++) {
				echo "this.lookbookitem++;";
			} 
			?>

		} else {
			this.lookbookitem--;
		}
		
	},
	right: function() {
		
		if (this.lookbookitem === <?php echo $counterviewminus; ?> ) {
			<?php 
						for ($x = 1; $x < $counterviewminus; $x++) {
				echo "this.lookbookitem--;";
			} 
			?>

		} else {
			this.lookbookitem++;
		}
		
	},
	makearrowsvisible: function() {
		this.arrowsvisible = true;
	},
	activatepreviewmode: function() {
		this.previewmode = true;
		this.arrowsvisible = false;
	},
	deactivatepreviewmode: function() {
		this.previewmode = false;
		this.arrowsvisible = true;

	}
}
});
</script>