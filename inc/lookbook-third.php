<div class="products-content products-content3">
                                    <?php 

                                        
                                        $sku3Trimmed = substr($sku3, 0, -6); 
                                        $lookbook_parent_id3 = wc_get_product_id_by_sku($sku3Trimmed);
                                        $lookbook_variation_id3 = wc_get_product_id_by_sku($sku3);
                                        $post3   = get_post($lookbook_parent_id3);
                                        $title3 = $post3->post_title;

                                        $product_variation3 = new WC_Product_Variation( $lookbook_variation_id3 );
                                                $variation_image3 = $product_variation3->get_image();

                                                $variation_price3 = $product_variation3->get_price_html();
                                                $variation_url3 = $product_variation3->get_permalink();
                                                $variation_color3 = $product_variation3->get_attribute('colors');
                                            
                                    ?>
                                    <a href="<?php echo $variation_url3; ?> ">

                                    <?php  echo $variation_image3; ?>

                                        <h4><?php  echo $title3; ?></h4>
                                        <h5><?php echo $variation_color3; ?></h5>
                                        <div>
                                            <p> <?php echo $variation_price3; ?></p>
                                        </div>
                                        </a>
                            </div>